demo_render = {}

local dir = 1


function demo_render.init()
    demo_render.tex_wood  = GameLib.createTexture("textures/wood64.png")
    demo_render.anim_cube = GameLib.createMesh("models/animated_cube.dae")
    print("in demo render => texture: " .. demo_render.tex_wood)
    GameLib.setClearCol(0.5, 0.6, 0)
    demo_render.pos_x = 10.0

end

function clear()
    GameLib.setClearCol(0.9, 0.6, 0.2)
    GameLib.clearBackBuffer()
end

function demo_render.postRender()

end

function demo_render.render()
    --GameLib.drawGrid()

    clear()
    --GameLib.setClearCol(0.5, 0.6, 0)
    --GameLib.clearBackBuffer()
    demo_render.pos_x = demo_render.pos_x + 200 * GameLib.frameTime() * dir

    if demo_render.pos_x > 1960 then
        dir = -1
    end
    if demo_render.pos_x < 32 then
        dir = 1
    end

    GameLib.drawTexture(demo_render.tex_wood, demo_render.pos_x, 440, 64, 64)


    GameLib.drawTexture(demo_render.tex_wood, demo_render.pos_x + 20, 240, 64, 64)
end


return demo_render