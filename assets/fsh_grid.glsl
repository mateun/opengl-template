#version 430

layout(location = 100) uniform vec4 grid_color;

out vec4 frag_color;

void main() {
    frag_color = grid_color;
}
