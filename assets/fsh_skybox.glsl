#version 430

layout(binding = 0) uniform sampler2D diffuseTexture;


out vec4 frag_color;
in vec2 fs_uv;

void main() {
    frag_color = texture(diffuseTexture, fs_uv);
}
