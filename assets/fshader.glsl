#version 430

layout(binding = 0) uniform sampler2D diffuseTexture;
layout(binding = 1) uniform sampler2D shadowMapTexture;
layout(binding = 2) uniform sampler2D skyboxTexture;


layout(location = 100) uniform bool useSingleColor;
layout(location = 101) uniform vec4 singleColor;
layout(location = 102) uniform bool isLit;
layout(location = 103) uniform vec3 lightDirection;
layout(location = 104) uniform bool noShadow;
layout(location = 105) uniform bool useTint;
layout(location = 106) uniform vec4 tintColor;

layout(location = 11) uniform bool isDepthPass;

out vec4 frag_color;
in vec2 fs_uv;
in vec3 fs_normal;
in vec4 fragment_pos_light;
in vec3 fragment_pos_camera;
in vec4 fs_weights;

float calc_shadow_factor(vec4 fragpos) {
    vec3 fpos_ndc = fragpos.xyz / fragpos.w;
    fpos_ndc = fpos_ndc * 0.5 + 0.5;
    float closestDepth = texture(shadowMapTexture, fpos_ndc.xy).r;
    float bias = 0.001;
    if (fpos_ndc.z - bias > closestDepth) 
    {
        return 0.5;
    }
    else if (fpos_ndc.z > 1.0)
    {
        return 1.0;
    }        
    else 
    {
        return 1.0;
    }
}


void main() {
    if (!isDepthPass) {
        if (useSingleColor) {
            frag_color = singleColor;
        }
        else {
            frag_color = texture(diffuseTexture, fs_uv);
            if (useTint) {
                frag_color *= tintColor;
            }
        }

        if (isLit) {
            vec3 lightVector = normalize(lightDirection)*-1;
            float diffuse = max(dot(fs_normal, lightVector), 0.1);
            float shadowFactor = 1.0;
            if (!noShadow) {
                shadowFactor = calc_shadow_factor(fragment_pos_light);
            }
            
            float wbefore = frag_color.w;
            frag_color.rgb = frag_color.rgb * diffuse * shadowFactor;
            frag_color.w = wbefore;

            // fog
            {
                vec4 fogColor = //texture(skyboxTexture, fs_uv);
                    vec4(0.81, 0.80, 0.722, 1);
                float fogStart = 15;
                float fogEnd = 100;
                float dist = length(fragment_pos_camera.xyz);
                float fogFactor = clamp(((fogEnd - dist) / (fogEnd - fogStart)), 0, 1);
                //frag_color = mix(fogColor, frag_color, fogFactor);
                
             
            }
           
            // Temporary to use for 
            // joint weight viz.
            // remove asap
            //frag_color = fs_weights;
            //frag_color.a = wbefore;
            // end temp
        }

        
    }
   
}
