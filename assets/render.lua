-- The engine expects a script called "render.lua"
-- in the root of the assets folder.
-- The user is free to require modules which
-- carry the actual meat of the rendering implementation.
-- The methodology of rendering is "immediate" rendering,
-- meaning there is no retained structures but
-- everything is done in the render method in an active
-- and immediate way.

-- Require our actual renderer to
-- avoid pollution of this engine hooked script.
r = require "demo_render"

function init()
    print("in init of render")
    package.loaded["demo_render"]=nil -- this makes `require` forget about its cache
    r =  require("demo_render")
    r.init()
end

function render()
    r.render()
end

-- This is called after the main offscreen texture has been rendered to.
-- Here postprocess effects might be applied.
function postRender()
    r.postRender();
end


-- Every script is read in from the engine
-- on startup.
-- We can write any function call here in the global
-- namespace which will then effectively be called.
init()





