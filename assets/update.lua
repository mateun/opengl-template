

-- Demonstrates how to require another module
-- To the engine itself, only the update.lua script is known.
-- This means we can and probably should/for the sake of
-- better modularity move out the actual meat of the implementation
-- into seperate modules and just let update.lua serve as
-- the bridge to the engine which it knows to call.
local gs2 = require "gamescript2"

counter = 1
local foobar = 0

function update()
    counter = counter + 1

    -- Note that the GameLib is an implicit module which is always available,
    -- as it gets injected by the engine.
    -- For a list of exposed functions see ... TODO
    --GameLib.setClearCol(0.8, 0.4, 0.2)
    gs2.another_update()

end
