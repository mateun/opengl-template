#version 430

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uv;


layout(location = 6) uniform mat4 model;
layout(location = 7) uniform mat4 view;
layout(location = 8) uniform mat4 proj;

layout(location = 12) uniform bool flipUVs;

uniform mat4 boneTransforms[100];

out vec2 fs_uv;

void main() {

    
    
    vec4 posc = proj * view * model * vec4(pos, 1);

    gl_Position = posc.xyww;

    fs_uv = uv;

    if (flipUVs) {
        fs_uv.y = 1 - fs_uv.y;
    }

    
    

}
