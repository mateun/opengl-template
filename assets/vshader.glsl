#version 430

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec4 weights;
layout(location = 4) in ivec4 boneIds;

layout(location = 6) uniform mat4 model;
layout(location = 7) uniform mat4 view;
layout(location = 8) uniform mat4 proj;

layout(location = 9) uniform mat4 mat_view_light;
layout(location = 10) uniform mat4 mat_proj_light;

layout(location = 11) uniform bool isDepthPass;
layout(location = 12) uniform bool flipUVs;
layout(location = 13) uniform bool isTextureAtlas;
layout(location = 14) uniform int atlasX;
layout(location = 15) uniform int atlasY;
layout(location = 16) uniform float atlasCols;
layout(location = 17) uniform float atlasRows;
layout(location = 18) uniform bool isSkeletalAnimation;

uniform mat4 boneTransforms[100];

out vec2 fs_uv;
out vec3 fs_normal;
out vec4 fragment_pos_light;
out vec3 fragment_pos_camera;
out vec4 fs_weights;


void main() {

    
    if (isSkeletalAnimation) {
        mat4 boneTransform = boneTransforms[boneIds[0]] * weights[0];
        boneTransform += boneTransforms[boneIds[1]] * weights[1];
        boneTransform += boneTransforms[boneIds[2]] * weights[2];
        boneTransform += boneTransforms[boneIds[3]] * weights[3];

        gl_Position = proj * view * boneTransform * vec4(pos, 1);
    }
    else {
        gl_Position = proj * view * model * vec4(pos, 1);
    }
    

    if (!isDepthPass) {
        fs_normal = normalize(vec3(model * vec4(normal, 0.0)));
        fs_uv = uv;

        // Test uv atlas calculation:
        if (isTextureAtlas) {
            float factorX = 1.0f / atlasCols;
            float factorY = 1.0f / atlasRows;
            fs_uv.x *= factorX;
            fs_uv.y *= factorY;

            fs_uv.x += factorX * atlasX;
            fs_uv.y += 1-(factorY * (atlasY+1));
        }

        if (flipUVs) {
            fs_uv.y = 1 - fs_uv.y;
        }

        fragment_pos_light = mat_proj_light * mat_view_light * model * vec4(pos, 1);
        fragment_pos_camera = (view * model * vec4(pos, 1)).xyz;
        fs_weights = weights;
    }
    

}
