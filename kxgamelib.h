//
// Created by mgrus on 31/12/2021.
//

#ifndef KOENIG012022_KXGAMELIB_H
#define KOENIG012022_KXGAMELIB_H

#include <string>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "ft2build.h"
#include FT_FREETYPE_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>
#include <map>
#include <mmsystem.h>
#include <dsound.h>
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <vector>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <lua/lua.h>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


class Quad;
static Quad* unitQuad = nullptr;
static Quad* getUnitQuad();

class GameData;
static GameData* gameData = nullptr;
GameData* getGameData();

class Texture;
static std::map<int, Texture*> textureStore;

// Lua

class LuaWrapper {
public:
    LuaWrapper();
    virtual ~LuaWrapper();

    /**
     * Reads the contents from the given file.
     * Returns the contents as a string.
     * @param fileName  The lua file
     * @return the contents of the file as a string.
     */
    std::string getCodeFromFile(const std::string& fileName);
    void runFunction(const std::string& funcName);
    void runScript(const std::string& scriptFilePath);
    void registerNativeFunction(const std::string& luaNamespace, const std::string& functionName, lua_CFunction func);
    void beginNativeFunctionRegistration();
    void endNativeFunctionRegistration(const std::string &luaNamespace);

private:
    lua_State* L;


};

// End Lua


// Lua callable functions
namespace luafuncs {
    int setClearCol(lua_State *);
    int drawTexture(lua_State*);
    int clearBackBuffer(lua_State*);
    int createTexture(lua_State*);
    int frameTime(lua_State*);
    int applyPostPrcessEffect(lua_State*);
    int createMesh(lua_State* );

}


// End lua exposed functions


#define GLERR  \
{ GLenum err = glGetError(); \
if (err != 0) { \
SDL_Log("gl error %d\n", err); \
exit(1); \
} }

std::string getAssetBasePath();
std::string f_read_file(const std::string& fileName);
GLuint f_createShader(const std::string& vertexShaderSource, const std::string& fragmentShaderSource);

// from: https://de.wikipedia.org/wiki/Perlin-Noise
float lerp(float a0, float a1, float x);
float perlin(float x, float y);



class Level;
struct ScreenData {
    int width = 800;
    int height = 600;
    bool fullScreen = false;
};
class GameData;
class Game {
public:
    virtual void init(GameData* gameData);
    virtual ScreenData getScreenData() ;
    virtual Level* getNextLevel() = 0;

protected:
    GameData* gameData;
};

class FrameBuffer;
class LuaBasedGame : public Game {
public:
    void init(GameData *gameData) override;
    ScreenData getScreenData() override;
    Level * getNextLevel() override;
private:
    LuaWrapper* lua = nullptr;


};

class Shader {
public:
    Shader(GLuint h) { _handle = h; }
    GLuint handle() { return _handle; }

private:
    GLuint _handle;
};

class ShaderStore {

public:
    void put(const std::string& vshName, const std::string& fsName);
    GLuint get(const std::string& vshName, const std::string& fsName);

private:
    std::map<std::string, Shader*> _shaders;
};

struct GameData {
    int screenWidth = 0;
    int screenHeight = 0;
    int logicalScreenWidth = 0;
    int logicalScreenHeight = 0;
    bool fullScreen = false;
    ShaderStore* shaderStore;
    SDL_Window* sdlWindow;
    uint64_t performanceFrequency;
    LPDIRECTSOUND8 dsound8;
    double lastFrameTime = 0;
    void* userData1 = nullptr;

    HWND nativeWindowsHandle() {
        SDL_SysWMinfo wmInfo;
        SDL_VERSION(&wmInfo.version);
        SDL_GetWindowWMInfo(sdlWindow, &wmInfo);
        return wmInfo.info.win.window;
    }

};

/**
* A level represents the current state
* of the game for which logic is calculated
* and its state is rendered to the screen.
*
* A game consists of several levels.
*
* Each level has its own update and render function.
* At the end of each frame, the engine asks the current level
* whether it is done and what the next level should be.
*
* The ending level can also pass back data which needs to be passed
* to the next levels init function.
*/
class Level {

public:
    Level(GameData* gameData) :_gameData(gameData) {}
    virtual ~Level() { if (_gameData) delete _gameData; }
    virtual void init(void* data) = 0;
    virtual bool isFinished() = 0;
    virtual void cleanUp() = 0;
    virtual void update(float frameTimeInSeconds, std::vector<SDL_Event>) = 0;
    virtual void render() = 0;

protected:
    GameData* _gameData = nullptr;
};

class Quad;
class Camera;
class DummyLevel : public Level {
public:
    DummyLevel(GameData* gameData);
    void init(void *data) override;
    bool isFinished() override;
    void cleanUp() override;
    void update(float frameTimeInSeconds, std::vector<SDL_Event>) override;
    void render() override;
private:
    LuaWrapper* lua = nullptr;
    GLuint shader = 0;

    Quad* _unitQuad;
    GLuint _shader;
    Texture* _mainColorTex;
    Texture* _mainDepthTex;
    Texture* _shadowDepthTex;
    FrameBuffer* _mainFBO;
    FrameBuffer* _shadowDepthFBO;
    Camera* _camera;
};

class FrameBuffer {
public:
    FrameBuffer(int w, int h);
    void attachColorTexture(Texture* tex);
    void attachDepthTexture(Texture* tex);
    void activateDrawBuffer();
    void deactivateDrawBuffer();
    void deactivateReadBuffer();
    void bind();
    void unbind();
    void clear(GLfloat* clearColor);
    bool check();


private:
    int _width;
    int _height;
    GLuint _handle;
};

class Grid {
public:

    Grid(int nrH, int nrV, float hDist, float vDist, glm::vec4 color);

    // To render a grid, we want to have a color only shader.
    // Should we let the user provide this ?
    // Or is it not better to actually provide this internally?
    // Like within the constructor build it?
    void draw(glm::mat4 model, glm::mat4 view, glm::mat4 proj);

private:
    int horizontalLines;
    int verticalLines;
    float horizontalDistance;
    float verticalDistance;
    GLuint vao;
    GLuint shader;
    glm::vec4 color;
    int centerlineOffset = 0;
};

class Camera;
class Texture;
class SceneData {
public:
    SceneData();
    glm::vec3 getLightDirection();
    void setCamera(Camera* c) { _camera = c; }
    Camera* getCamera() { return _camera; }
    void setShadowCamera(Camera* c) { _shadowCamera = c; }
    Camera* getShadowCamera() { return _shadowCamera; }
    bool isDepthPass() { return _depthPass; }
    void setDepthPass(bool val) { _depthPass = val; }
    Texture* getShadowDepthTexture() { return _shadowDepthTexture;  }
    void setShadowDepthTexture(Texture* tex) { _shadowDepthTexture = tex; }
    void setFlipUVs(bool val) { _flipUVs = val; }
    bool isFlipUVs() { return _flipUVs; }
    void setLit(bool val) { _lit = val; }
    bool isLit() { return _lit; }
    bool isTextureAtlas() { return _textureAtlas; }
    void setTextureAtlas(bool val) { _textureAtlas = val; }
    glm::vec2 getAtlasOffset() { return _atlasOffset; }
    void setAtlasOffset(glm::vec2 offset) { _atlasOffset = offset; }
    glm::vec2 getAtlasDimension() { return _atlasDimension; }
    void setAtlasDimension(glm::vec2 val) { _atlasDimension = val; }
    void useSingleColor(bool val) { _singleColorYN = val; }
    bool isSingleColor() { return _singleColorYN; }
    void setSingleColor(glm::vec4 col) { _singleColor = col; }
    glm::vec4 getSingleColor() { return _singleColor; }
    void setExplicitLightDirection(glm::vec3);
    void useTint(bool val) { _useTint = val; }
    bool isUsingTint() { return _useTint; }
    void setTint(glm::vec4 t) { _tint = t; }
    glm::vec4 getTint() { return _tint; }


private:
    Camera* _camera = nullptr;
    Camera* _shadowCamera = nullptr;

    bool _depthPass;
    bool _flipUVs;
    bool _textureAtlas;
    bool _lit;
    bool _singleColorYN = false;
    bool _useTint = false;

    glm::vec2 _atlasOffset = glm::vec2(0);
    glm::vec2 _atlasDimension = glm::vec2(1, 1);

    Texture* _shadowDepthTexture = nullptr;

    glm::vec4 _singleColor;
    glm::vec4 _tint;

    glm::vec3 _explicitLightDirection;


};


class Camera {

public:
    Camera(glm::mat4 matProj, glm::mat4 matView);
    Camera(glm::mat4 matProj, glm::vec3 location, glm::vec3 forward);
    void updateViewMatrix(glm::vec3 location, glm::vec3 forward);
    void setOrtho(bool val) { _orthographic = val; }
    bool isOrtho() { return _orthographic; }
    glm::mat4 getProjectionMatrix() { return _matProj; };
    glm::mat4 getViewMatrix() { return _matView; }
    glm::vec3 location() { return _location; }
    glm::vec3 forward() { return _forward; }
    glm::vec3 right();
    glm::vec3 up();

private:
    glm::vec3 _location;
    glm::vec3 _forward;
    glm::mat4 _matView;
    glm::mat4 _matProj;
    bool _orthographic;

};

class Ray {
public:
    void initWithScreenCoordinates(glm::vec2 mouseCoords, GameData* gd, Camera* camera);
    bool intersectsPlane(glm::vec3 planeNormal, glm::vec3 planePoint, glm::vec3* intersectionPoint);
    bool intersectsSphere(glm::vec3 sphereCenter, float radius);
    glm::vec3 origin() { return _origin; }
    glm::vec3 dir() { return _direction; }
    glm::vec4 eye() { return _eyeSpace; }
    glm::vec4 clip() { return _clipSpace; }
    void setOrigin(glm::vec3 o) { _origin = o; }
    void setDirection(glm::vec3 d) { _direction = d; }


private:
    glm::vec3 _origin;
    glm::vec3 _direction;
    glm::vec4 _eyeSpace;
    glm::vec4 _clipSpace;
    float maxLength;
};

struct WaveHeaderType
{
    char chunkId[4];
    unsigned long chunkSize;
    char format[4];
    char subChunkId[4];
    unsigned long subChunkSize;
    unsigned short audioFormat;
    unsigned short numChannels;
    unsigned long sampleRate;
    unsigned long bytesPerSecond;
    unsigned short blockAlign;
    unsigned short bitsPerSample;
    char dataChunkId[4];
    unsigned long dataSize;
};
struct Sound {
    LPDIRECTSOUNDBUFFER8 secondaryBuffer;
    const char* name;
};
Sound* createSoundFromWaveFile(const char* fileName, GameData* gameData);
void playSound(Sound*, bool loop, long volume = -1000);
void stopSound(Sound* s);

glm::vec2 getTileCoordsForLetter(char letter);
glm::vec2 getTileCoordsForLetter256(char letter);

FT_Library* getFreeType();

class GFont {

public:
    GFont(const std::string& fontName, FT_Library* ftlib, int fontSize);

public:
    FT_Face& handle;

private:
    FT_Face _face;
    FT_Library* _ftlib;
};

class Texture;
/**
*   This class represents a piece of (one line) text
*   which can be rendered as a texture.
*
*/
struct TextObject {
    TextObject(GFont* face);
    void setText(const std::string& text);
    void renderTextToBuffer(const std::string& text);

    GFont* _face = nullptr;
    Texture* _freeTextTexture = nullptr;
    uint8_t* _pixelBuffer = nullptr;
    int w;
    int h;
    GLuint pbos[2];
    int index = 1;
    std::string _oldText;

};
enum TextureUsage {
    Color,
    Depth
};
class Texture {
public:
    Texture();
    virtual ~Texture();
    Texture(int width, int height, TextureUsage usage, GLint filterParam = GL_LINEAR);
    void setData(uint8_t* pixels);
    void setUsageParameters(TextureUsage usage);
    void setDimension(int w, int h);
    GLuint handle() { return _textureId; }
    void bindToUnit(int unit);
    void unbind();
    const int width;
    const int height;


private:
    int _width;
    int _height;
    GLuint _textureId;

};


class GameData;

// Proc

struct CubeObj
{
    GLuint vao;
    int numberOfVertices = 0;
};

CubeObj createCube();

void renderCube(CubeObj cube, GLuint shader, glm::mat4 world, glm::vec4 color, SceneData* sceneData);



// OOP

class Geometry {
public:
    GLuint handle() { return _vao; }
    virtual void render(GLuint shader, glm::mat4 world, Texture* tex, SceneData* sceneData);
    virtual void render(GLuint shader, glm::mat4 world, glm::vec4 color, SceneData* sceneData);

protected:
    virtual void renderCommon(glm::mat4 world, SceneData* sd);

protected:

    GLuint _vao;
    int numberOfTriangles = 0;
};

class Quad : public Geometry {
public:
    Quad();
    void renderCommon(glm::mat4, SceneData*) override;
};


class Cube : public Geometry {
public:
    Cube();
};


class Circle : public Geometry {
public:
    Circle(int segments);
    void renderCommon(glm::mat4, SceneData*) override;
};

struct VertexWeight {
    uint32_t vertexId;
    float weight;
};

struct Bone
{
    char* name = nullptr;
    glm::mat4 offsetMatrix;
    std::vector<VertexWeight> vertexWeights;
};

struct KeyFrame {
    Bone* bone;
    double time;
    glm::quat rotation;

};

struct Animation {
    std::string name;
    double duration = 0;
    double ticksPerSecond  = 0;
    std::vector<KeyFrame*> keyFrames;
};







class Mesh : public Geometry{
public:
    Mesh(GameData*);
    void initWithModel(const std::string& modelFileName);
    void setInstanceWorldMatrices(const std::vector<glm::mat4>& worldMatrices);
    void renderInstanced(GLuint shader, std::vector<glm::mat4> worldMatrices, Texture* texture, SceneData* sd);
    void render(GLuint shader, glm::mat4 world, Texture* meshTex, SceneData* sd) override;
    bool isSkeletal() { return _isSkeletalMesh; }
    const std::vector<Bone*>& bones() { return _bones; }

protected:
    //GLuint _vao;
    std::vector<GLuint> _vaos;
    std::vector<int> _numbersOfVertices;
    std::vector<Texture*> _defaultTextures;
    GameData* _gameData;
    bool _isSkeletalMesh = false;
    std::vector<Bone*> _bones;
    std::vector<glm::mat4> _instanceWorldMatrices;



};

//////////////////////////////////////////////////////////////////////
// IMPL
//////////////////////////////////////////////////////////////////////

//#define KXGAMELIB_IMPL
#ifdef KXGAMELIB_IMPL

#include <string>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <vector>

static char* sdlBasePath = nullptr;
static std::string bps ="";

std::string getAssetBasePath()
{
    if (sdlBasePath == nullptr) {

        // TODO remove this whole copy back-and-forth,
        // this is just experimental when hunting down a leak issue.
        char* temp = SDL_GetBasePath();
        sdlBasePath = (char*)malloc(strlen(temp) + 1);
        if (sdlBasePath != nullptr) {
            memcpy_s(sdlBasePath, strlen(temp), temp, strlen(temp));
            sdlBasePath[strlen(temp)] = '\0';
            bps = std::string(sdlBasePath);
            SDL_free(temp);
        }

#ifdef WIN32
        bps += std::string("../assets/");;
#else
        bps += "../assets/";
#endif

    }

    return bps;
}

std::string f_read_file(const std::string& fileName)
{
    std::string result = "";

    SDL_Log("fileName: %s", fileName.c_str());
    FILE* f = fopen(fileName.c_str(), "rb");
    fseek(f, 0, SEEK_END);
    SDL_Log("after seek.");
    int size = ftell(f);
    rewind(f);

    char* buf = new char[size + 1];
    memset(buf, 0, sizeof(buf));
    int read = fread(buf, 1, size, f);
    if (read != size) {
        SDL_Log("error reading file\n.");
        exit(1);
    }
    buf[size] = '\0';

    result = buf;
    fclose(f);
    return result;

}


GLuint f_createShader(const std::string& vertexShaderSource, const std::string& fragmentShaderSource) {
    GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vssource_char = vertexShaderSource.c_str();
    glShaderSource(vshader, 1, &vssource_char, NULL);
    glCompileShader(vshader);
    GLint compileStatus;
    glGetShaderiv(vshader, GL_COMPILE_STATUS, &compileStatus);
    if (GL_FALSE == compileStatus) {

        GLint logSize = 0;
        glGetShaderiv(vshader, GL_INFO_LOG_LENGTH, &logSize);
        std::vector<GLchar> errorLog(logSize);
        glGetShaderInfoLog(vshader, logSize, &logSize, &errorLog[0]);
        //    result.errorMessage = errorLog.data();
        SDL_Log("vshader error: %s", errorLog.data());
        glDeleteShader(vshader);
        //  return result;
        return 0;

    }


    GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fssource_char = fragmentShaderSource.c_str();
    glShaderSource(fshader, 1, &fssource_char, NULL);
    glCompileShader(fshader);

    glGetShaderiv(fshader, GL_COMPILE_STATUS, &compileStatus);
    if (GL_FALSE == compileStatus) {
        GLint logSize = 0;
        glGetShaderiv(fshader, GL_INFO_LOG_LENGTH, &logSize);
        std::vector<GLchar> errorLog(logSize);
        glGetShaderInfoLog(fshader, logSize, &logSize, &errorLog[0]);
        //   result.errorMessage = errorLog.data();
        SDL_Log("fragment shader error: %s", errorLog.data());
        glDeleteShader(fshader);
        return 0;

    }

    GLuint p = glCreateProgram();
    glAttachShader(p, vshader);
    glAttachShader(p, fshader);
    glLinkProgram(p);

    glGetProgramiv(p, GL_LINK_STATUS, &compileStatus);

    if (GL_FALSE == compileStatus) {
        GLint maxLength = 0;
        glGetProgramiv(p, GL_INFO_LOG_LENGTH, &maxLength);
        std::vector<GLchar> infoLog(maxLength);
        glGetProgramInfoLog(p, maxLength, &maxLength, &infoLog[0]);
        //    result.ok = false;
      //      result.errorMessage = infoLog.data();
        SDL_Log("shader linking error: %s", infoLog.data());
        glDeleteProgram(p);
        glDeleteShader(vshader);
        glDeleteShader(fshader);

    }

    GLenum err = glGetError();
    if (err != 0) {
        return 0;
    }

    glDeleteShader(vshader);
    glDeleteShader(fshader);

    return p;

}

// From: https://de.wikipedia.org/wiki/Perlin-Noise
float lerp(float a0, float a1, float x)
{
    if (x < 0.0) return a0;
    if (x > 1.0) return a1;
    return (a1 - a0) * x + a0;
    //return (a1 - a0) * (3.0 - x * 2.0) * x * x + a0; // Alternative: Kubische Interpolation mit dem Polynom 3 * x^2 - 2 * x^3
    //return (a1 - a0) * ((x * (x * 6.0 - 15.0) + 10.0) * x * x * x) + a0; // Alternative:  Interpolation mit dem Polynom 6 * x^5 - 15 * x^4 + 10 * x^3
}

// From: https://de.wikipedia.org/wiki/Perlin-Noise
glm::vec2 randomGradient(int ix, int iy)
{
    const unsigned w = 8 * sizeof(unsigned);
    const unsigned s = w / 2;
    unsigned a = ix, b = iy;
    a *= 3284157443;
    b ^= a << s | a >> w - s;
    b *= 1911520717;
    a ^= b << s | b >> w - s;
    a *= 2048419325;
    float random = a * (3.14159265 / ~(~0u >> 1)); // randon number in interval [0, 2 * Pi]
    glm::vec2 v;
    v.x = sin(random);
    v.y = cos(random);
    return v;

}

// From: https://de.wikipedia.org/wiki/Perlin-Noise
static float dotGridGradient(int ix, int iy, float x, float y)
{
    // Gradient at integer coordinates
    glm::vec2 gradient = randomGradient(ix, iy);
    // The distance vector
    float dx = x - (float)ix;
    float dy = y - (float)iy;
    return dx * gradient.x + dy * gradient.y; // Skalarprodukt
}

// From: https://de.wikipedia.org/wiki/Perlin-Noise
float perlin(float x, float y)
{
    // Coordinates of the 4 corner coordinates
    int x0 = (int)floor(x);
    int x1 = x0 + 1;
    int y0 = (int)floor(y);
    int y1 = y0 + 1;
    // Weight for the interpolation
    float sx = x - (float)x0;
    float sy = y - (float)y0;

    // Interpolate the 4 corner gradients
    float n0, n1, ix0, ix1, value;
    n0 = dotGridGradient(x0, y0, x, y);
    n1 = dotGridGradient(x1, y0, x, y);
    ix0 = lerp(n0, n1, sx);
    n0 = dotGridGradient(x0, y1, x, y);
    n1 = dotGridGradient(x1, y1, x, y);
    ix1 = lerp(n0, n1, sx);
    return lerp(ix0, ix1, sy);
}

SceneData::SceneData()
{
    _depthPass = false;
    _flipUVs = true;
    _lit = true;
    _textureAtlas = false;
}
glm::vec3 SceneData::getLightDirection() {
    if (_shadowCamera) {
        return _shadowCamera->forward();
    }
    else {
        return _explicitLightDirection;
    }

}
void SceneData::setExplicitLightDirection(glm::vec3 lightDir)
{
    _explicitLightDirection = lightDir;
}
std::string getKey(const std::string& vshName, const std::string& fsName) {
    return  vshName + "_" + fsName;
}
void ShaderStore::put(const std::string& vshName, const std::string& fsName)
{

    std::string key = getKey(vshName, fsName);
    if (_shaders[key]) {
        return;
    }

    std::string basePath = getAssetBasePath();
    SDL_Log("basePath: %s", basePath.c_str());
    std::string vshaderSource = f_read_file(basePath + vshName);
    std::string fshaderSource = f_read_file(basePath + fsName);
    GLuint shader = f_createShader(vshaderSource, fshaderSource);
    if (shader == 0) {
        exit(2);
    }

    _shaders[key] = new Shader(shader);

}
GLuint ShaderStore::get(const std::string& vshName, const std::string& fsName)
{

    Shader* shader = _shaders[getKey(vshName, fsName)];
    if (shader) {
        return shader->handle();
    }
    else {
        return 0;
    }

}


Grid::Grid(int nrH, int nrV, float hDist, float vDist, glm::vec4 color)
        : horizontalLines(nrH), verticalLines(nrV),
          horizontalDistance(hDist), verticalDistance(vDist), color(color)
{

    std::string basePath = getAssetBasePath();
    std::string vshaderSource = f_read_file(basePath + "vsh_grid.glsl");
    std::string fshaderSource = f_read_file(basePath + "fsh_grid.glsl");
    this->shader = f_createShader(vshaderSource, fshaderSource);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vboPos;
    float* positions = new float[(horizontalLines * 2 * 3) + (verticalLines * 2* 3) + 2 * 2 * 3];
    int posIndex = 0;
    int firstH = -horizontalLines / 2 * horizontalDistance;
    int firstV = -verticalLines / 2 * verticalDistance;
    for (int h = 0; h < horizontalLines; h++) {
        // Start point
        positions[posIndex++] = -50;    // x
        positions[posIndex++] = -0.01;                              // we are on the ground, y = zero
        positions[posIndex++] = firstH + h * horizontalDistance;    // z

        // End point
        positions[posIndex++] = 50;                            // x
        positions[posIndex++] = -.01;                              // we are on the ground, y = zero
        positions[posIndex++] = firstH + h * horizontalDistance;    // z

    }

    for (int v = 0; v < verticalLines; v++) {
        // Start point V
        positions[posIndex++] = firstV + v * verticalDistance; ;    // x
        positions[posIndex++] = -.02;                              // we are on the ground, y = zero
        positions[posIndex++] = 50;   // z

        // End point vertical
        positions[posIndex++] = firstV + v * verticalDistance; ;                            // x
        positions[posIndex++] = -.02;                              // we are on the ground, y = zero
        positions[posIndex++] = -50;  // z
    }

    centerlineOffset = posIndex;

    // Add vertices for center lines, horizontal and vertical
    positions[posIndex++] = -50;
    positions[posIndex++] = 0.01;
    positions[posIndex++] = 0;
    positions[posIndex++] = 50;
    positions[posIndex++] = 0.01;
    positions[posIndex++] = 0;

    positions[posIndex++] = 0;
    positions[posIndex++] = 0.01;
    positions[posIndex++] = 50;
    positions[posIndex++] = 0;
    positions[posIndex++] = 0.01;
    positions[posIndex++] = -50;
    // End centerlines


    int sizeofCenterLines = 12 * 4;
    int sizeOfHLines = horizontalLines * 6 * 4;
    int sizeOfVLines = verticalLines * 6 * 4;

    glGenBuffers(1, &vboPos);
    glBindBuffer(GL_ARRAY_BUFFER, vboPos);
    glBufferData(GL_ARRAY_BUFFER, sizeOfHLines + sizeOfVLines + sizeofCenterLines, positions, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);


    GLERR

    glBindVertexArray(0);
}

void Grid::draw(glm::mat4 model, glm::mat4 view, glm::mat4 proj) {
    glBindVertexArray(vao);
    glUseProgram(shader);

    glUniformMatrix4fv(6, 1, false, glm::value_ptr(model));
    glUniformMatrix4fv(7, 1, false, glm::value_ptr(view));
    glUniformMatrix4fv(8, 1, false, glm::value_ptr(proj));
    // Set the grid color
    glUniform4fv(100, 1, glm::value_ptr(color));

    glDrawArrays(GL_LINES, 0, (horizontalLines * 2) + (verticalLines * 2));

    glm::vec4 centerColor = { .7, .7, .7, 1 };
    glUniform4fv(100, 1, glm::value_ptr(centerColor));
    glDrawArrays(GL_LINES, centerlineOffset / 3, 4);

    GLERR
}

void Ray::initWithScreenCoordinates(glm::vec2 mouseCoords, GameData* gd, Camera* camera)
{
    mouseCoords.y = gd->screenHeight - mouseCoords.y;
    float x = (2.0f * mouseCoords.x) / gd->screenWidth - 1.0f;
    float y = (2.0f * mouseCoords.y) / gd->screenHeight - 1.0f;
    float z = 1.0f;
    glm::vec3 ray_nds = glm::vec3(x, y, z);
    glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0, 1.0);
    //printf("ray clip: %f/%f\n", ray_clip.x, ray_clip.y);
    glm::vec4 ray_eye = glm::inverse(camera->getProjectionMatrix()) * ray_clip;
    ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);
    //printf("ray eye: %f/%f\n", ray_eye.x, ray_eye.y);
    glm::vec3 ray_wor = glm::inverse(camera->getViewMatrix()) * ray_eye;
    ray_wor = normalize(ray_wor);
    this->_origin = camera->location();
    this->_direction = ray_wor;
    this->maxLength = 100;

    if (camera->isOrtho())
    {
        // for ortho projection things work a bit differently:
        this->_origin = camera->location() + glm::normalize(camera->right()) * ray_eye.x + glm::normalize(camera->up()) * ray_eye.y;
        this->_direction = normalize(camera->forward());
    }

    _eyeSpace = ray_eye;
    _clipSpace = ray_clip;

}
bool Ray::intersectsPlane(glm::vec3 planeNormal, glm::vec3 planePoint, glm::vec3* intersectionPoint)
{
    float rayPlaneOrientation = glm::dot(_direction, planeNormal);
    if (abs(rayPlaneOrientation) <= 0.0001f) {
        //SDL_Log("ray plane orientation too small. no collision! %f\n", rayPlaneOrientation);
        return false;
    }

    float t = (glm::dot((planePoint - _origin), planeNormal)) / rayPlaneOrientation;
    *intersectionPoint = _origin + t * _direction;

    return t > 0;
}
//
// Taken from here, just translated to glm:
// https://gamedev.stackexchange.com/questions/96459/fast-ray-sphere-collision-code
bool Ray::intersectsSphere(glm::vec3 sphereCenter, float radius)
{
    // Calculate ray start's offset from the sphere center
    glm::vec3 p = this->_origin - sphereCenter;

    float rSquared = radius * radius;
    float p_d = glm::dot(p, _direction);

    // The sphere is behind or surrounding the start point.
    if (p_d > 0 || glm::dot(p, p) < rSquared)
        return false;

    // Flatten p into the plane passing through c perpendicular to the ray.
    // This gives the closest approach of the ray to the center.
    glm::vec3 a = p - p_d * _direction;

    float aSquared = glm::dot(a, a);

    // Closest approach is outside the sphere.
    if (aSquared > rSquared)
        return false;

    // Calculate distance from plane where ray enters/exits the sphere.
    float h = sqrt(rSquared - aSquared);

    // Calculate intersection point relative to sphere center.
    glm::vec3 i = a - h * _direction;

    glm::vec3 intersection = sphereCenter + i;
    glm::vec3 normal = i / radius;
    // We've taken a shortcut here to avoid a second square root.
    // Note numerical errors can make the normal have length slightly different from 1.
    // If you need higher precision, you may need to perform a conventional normalization.

    //return (intersection, normal);
    return true;
}

Camera::Camera(glm::mat4 matProj, glm::mat4 matView)
{
    _matProj = matProj;
    _matView = matView;
    _forward = (matView)[2];
    _forward.z *= -1;
    _location = glm::inverse(matView)[3];
}
Camera::Camera(glm::mat4 matProj, glm::vec3 location, glm::vec3 forward)
{
    _matProj = matProj;
    _matView = glm::lookAt(location, location + forward, glm::vec3(0, 1, 0));
    _location = location;
    _forward = forward;
}
void Camera::updateViewMatrix(glm::vec3 location, glm::vec3 forward)
{
    _matView = glm::lookAt(location, location + glm::normalize(forward), glm::vec3(0, 1, 0));
    _location = location;
    _forward = forward;
}
glm::vec3 Camera::right()
{
    glm::vec3 right = glm::cross(_forward, glm::vec3(0, 1, 0));
    return glm::normalize(right);
}
glm::vec3 Camera::up()
{
    glm::vec3 up = glm::cross(right(), _forward);
    return glm::normalize(up);
}


static FT_Library ftLibrary;
static bool freeTypeInitalized = false;


static TextObject* fpsText;
static TextObject* frameTimeText;
static TextObject* startButtonText;
static TextObject* updateTimeText;

#pragma comment(lib, "freetyped.lib")

TextObject::TextObject(GFont* face) {
    _face = face;
    glGenBuffers(2, pbos);
    index = 0;
}


void TextObject::setText(const std::string& text) {

    // We are done when we know this text already
    if (_oldText == text) {
        return;
    }

    // If not, we need to clean up before we
    // setup everything for the new text:
    if (_freeTextTexture) {
        delete(_freeTextTexture);
        _freeTextTexture = nullptr;
    }
    if (_pixelBuffer) {
        free(_pixelBuffer);
        _pixelBuffer = nullptr;
    }

    w = _face->handle->size->metrics.x_ppem * text.size();
    h = _face->handle->size->metrics.y_ppem * 1.3;
    _freeTextTexture = new Texture(w, h, TextureUsage::Color);

    this->_pixelBuffer = (uint8_t*)malloc((int64_t)w * (int64_t)h * 4);
    if (_pixelBuffer) {
        memset(_pixelBuffer, 0, (int64_t)w * (int64_t)h * 4);
    }

    renderTextToBuffer(text);
    _freeTextTexture->setData(_pixelBuffer);

    _oldText = text;
}




bool initFreeType() {
    FT_Error error = FT_Init_FreeType(&ftLibrary);
    if (error) {
        return false;
    }
    return true;
}

void TextObject::renderTextToBuffer(const std::string& text)
{
    FT_GlyphSlot slot = _face->handle->glyph;
    int penX, penY, n;
    penX = 2;
    FT_Size size = _face->handle->size;

    for (n = 0; n < text.length(); n++) {

        // TODO glyph caching
        // see https://www.freetype.org/freetype2/docs/tutorial/step2.html#section-2

        FT_UInt glyph_index = FT_Get_Char_Index(_face->handle, text[n]);
        FT_Error error = FT_Load_Glyph(_face->handle, glyph_index, FT_LOAD_DEFAULT);
        if (error) {
            return;
        }
        if (FT_Render_Glyph(_face->handle->glyph, FT_RENDER_MODE_NORMAL)) {
            return;
        }

        // actually draw the bitmap into the texture
        FT_Bitmap bitmap = slot->bitmap;
        int bearingX = _face->handle->glyph->bitmap_left;
        int bearingY = _face->handle->glyph->bitmap_top;



        for (int y = 0; y < _face->handle->glyph->bitmap.rows; y++) {
            for (int x = 0; x < _face->handle->glyph->bitmap.width; x++) {
                int i = x + y * _face->handle->glyph->bitmap.width;
                int ip = penX + bearingX + x + ((size->metrics.y_ppem - bearingY + y) * w);
                uint8_t val = _face->handle->glyph->bitmap.buffer[i];

                _pixelBuffer[(4 * ip)] = val;
                _pixelBuffer[(4 * ip) + 1] = val;
                _pixelBuffer[(4 * ip) + 2] = val;
                _pixelBuffer[(4 * ip) + 3] = val;

            }
        }
        penX += (slot->advance.x / 64);
    }
}

GFont::GFont(const std::string& fontName, FT_Library* ftLib, int fontSize) : _ftlib(ftLib), handle(_face)
{
    FT_Error error = FT_New_Face(*_ftlib, fontName.c_str(), 0, &_face);
    //FT_Set_Char_Size(_face, fontSize * 64, 0, 96, 96);
    FT_Set_Pixel_Sizes(_face, fontSize, 0);
}

FT_Library* getFreeType()
{
    if (!freeTypeInitalized) {
        initFreeType();
        freeTypeInitalized = true;
    }
    return &ftLibrary;
}


// Get the absolute pixel values from the 256x256 font texture
// to use for the respective letter.
glm::vec2 getTileCoordsForLetter256(char letter)
{
    switch (letter)
    {
        case 'a':
        case 'A': return { 0, 0 };
        case 'b':
        case 'B': return { 1, 0 };
        case 'c':
        case 'C': return { 2, 0 };
        case 'd':
        case 'D': return { 3, 0 };
        case 'e':
        case 'E': return { 4, 0 };
        case 'f':
        case 'F': return { 5, 0 };
        case 'g':
        case 'G': return { 6, 0 };
        case 'h':
        case 'H': return { 7, 0 };
        case 'i': case 'I': return { 8, 0 };
        case 'j': case 'J': return { 9, 0 };
        case 'k': case 'K': return { 10, 0 };
        case 'l': case 'L': return { 11, 0 };
        case 'm': case 'M': return { 12, 0 };
        case 'n': case 'N': return { 13, 0 };
        case 'o': case 'O': return { 14, 0 };
        case 'p': case 'P': return { 15, 0 };

        case 'q': case 'Q': return { 16, 0 };
        case 'r': case 'R': return { 17, 0 };
        case 's': case 'S': return { 18, 0 };
        case 't': case 'T': return { 19, 0 };
        case 'u': case 'U': return { 20, 0 };
        case 'v': case 'V': return { 21, 0 };
        case 'w': case 'W': return { 22, 0 };
        case 'x': case 'X': return { 23, 0 };
        case 'y': case 'Y': return { 24, 0 };
        case 'z': case 'Z': return { 25, 0 };
        case '0': return { 26, 0 };
        case '1': return { 27, 0 };
        case '2': return { 28, 0 };
        case '3': return { 29, 0 };
        case '4': return { 30, 0 };
        case '5': return { 31, 0 };
        case '6': return { 0, 1 };
        case '7': return { 1, 1 };
        case '8': return { 2, 1 };
        case '9': return { 3, 1 };
        case '.': return { 4, 1 };
        case ',': return { 5, 1 };
        case ';': return { 6, 1 };
        case '/': return { 7, 1 };
        case '\\': return { 8, 1 };
        case ':': return { 9, 1 };
        case ' ': return { 10, 1 };
        case '-': return { 11, 1 };
        default: return { 11, 2 };
    }

}


// Get the absolute pixel values from the 512x512 font texture
// to use for the respective letter.
glm::vec2 getTileCoordsForLetter(char letter)
{
    switch (letter)
    {
        case 'a':
        case 'A': return { 0, 0 };
        case 'b':
        case 'B': return { 1, 0 };
        case 'c':
        case 'C': return { 2, 0 };
        case 'd':
        case 'D': return { 3, 0 };
        case 'e':
        case 'E': return { 4, 0 };
        case 'f':
        case 'F': return { 5, 0 };
        case 'g':
        case 'G': return { 6, 0 };
        case 'h':
        case 'H': return { 7, 0 };
        case 'i': case 'I': return { 8, 0 };
        case 'j': case 'J': return { 9, 0 };
        case 'k': case 'K': return { 10, 0 };
        case 'l': case 'L': return { 11, 0 };
        case 'm': case 'M': return { 12, 0 };
        case 'n': case 'N': return { 13, 0 };
        case 'o': case 'O': return { 14, 0 };
        case 'p': case 'P': return { 15, 0 };

        case 'q': case 'Q': return { 0, 1 };
        case 'r': case 'R': return { 1, 1 };
        case 's': case 'S': return { 2, 1 };
        case 't': case 'T': return { 3, 1 };
        case 'u': case 'U': return { 4, 1 };
        case 'v': case 'V': return { 5, 1 };
        case 'w': case 'W': return { 6, 1 };
        case 'x': case 'X': return { 7, 1 };
        case 'y': case 'Y': return { 8, 1 };
        case 'z': case 'Z': return { 9, 1 };
        case '1': return { 11, 1 };
        case '2': return { 12, 1 };
        case '3': return { 13, 1 };
        case '4': return { 14, 1 };
        case '5': return { 15, 1 };
        case '6': return { 0, 2 };
        case '7': return { 1, 2 };
        case '8': return { 2, 2 };
        case '9': return { 3, 2 };
        case '0': return { 4, 2 };
        case '.': return { 5, 2 };
        case ',': return { 6, 2 };
        case ';': return { 7, 2 };
        case '/': return { 8, 2 };
        case '\\': return { 9, 2 };
        case ':': return { 10, 2 };
        case ' ': return { 11, 2 };

        default: return { 128, 32 };
    }

}


Texture::Texture() : width(0), height(0)
{
    glGenTextures(1, &_textureId);

}
Texture::~Texture()
{
    glDeleteTextures(1, &_textureId);
}
void Texture::setDimension(int w, int h) {
    _width = w;
    _height = h;
}
void Texture::setUsageParameters(TextureUsage usage)
{
    glBindTexture(GL_TEXTURE_2D, _textureId);

    GLenum err = glGetError();

    if (usage == TextureUsage::Depth) {
        //glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, w, h);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
                     _width, _height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

    }
    else {
        glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, _width, _height);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    }
    err = glGetError();
    if (err) {
        exit(2);
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


    GLERR
    glBindTexture(GL_TEXTURE_2D, 0);
}
Texture::Texture(int w, int h, TextureUsage usage, GLint filterParam ) : _width(w), _height(h), width(w), height(h) {
    glGenTextures(1, &_textureId);
    glBindTexture(GL_TEXTURE_2D, _textureId);

    GLenum err = glGetError();

    if (usage == TextureUsage::Depth) {
        //glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, w, h);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
                     w, h, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

    }
    else {
        glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, w, h);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    }
    err = glGetError();
    if (err) {
        exit(2);
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterParam);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterParam);


    GLERR
    glBindTexture(GL_TEXTURE_2D, 0);

}
void Texture::setData(uint8_t* pixels) {
    glBindTexture(GL_TEXTURE_2D, _textureId);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, _width, _height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    GLERR
    glBindTexture(GL_TEXTURE_2D, 0);

}
void Texture::bindToUnit(int unit) {

    switch (unit) {
        case 0: glActiveTexture(GL_TEXTURE0); break;
        case 1: glActiveTexture(GL_TEXTURE1); break;
        case 2: glActiveTexture(GL_TEXTURE2); break;
    }
    glBindTexture(GL_TEXTURE_2D, _textureId);

}
void Texture::unbind() {
    glBindTexture(GL_TEXTURE_2D, 0);
}


Quad::Quad()
{
    glGenVertexArrays(1, &_vao);
    glBindVertexArray(_vao);

    GLuint vboPos;
    float positions[] = {
            -0.5,  0.5, 0,   // top left
            -0.5, -0.5, 0,   // bottom left
            0.5, -0.5, 0,   // bottom right
            0.5,  0.5, 0    // top right
    };


    GLuint vboIndices;
    GLuint indices[] = {
            0, 1, 2,
            0, 2, 3
    };

    GLuint vboUVs;
    GLfloat uvs[] = {
            0, 1,
            0, 0,
            1, 0,
            1, 1
    };

    GLuint vboNormals;
    GLfloat normals[] = {
            0, 0,1,
            0, 0,1,
            0, 0,1,
            0, 0,1,


    };

    glGenBuffers(1, &vboPos);
    glBindBuffer(GL_ARRAY_BUFFER, vboPos);
    glBufferData(GL_ARRAY_BUFFER, 12 * 4, positions, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glGenBuffers(1, &vboUVs);
    glBindBuffer(GL_ARRAY_BUFFER, vboUVs);
    glBufferData(GL_ARRAY_BUFFER, 8 * 4, uvs, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &vboNormals);
    glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
    glBufferData(GL_ARRAY_BUFFER, 12* 4, normals, GL_STATIC_DRAW);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(2);

    glGenBuffers(1, &vboIndices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * 4, indices, GL_STATIC_DRAW);

    GLERR

    glBindVertexArray(0);

    numberOfTriangles = 6;
}
Cube::Cube()
{
    glGenVertexArrays(1, &_vao);
    glBindVertexArray(_vao);

    GLuint vboPos;
    float positions[] = {
            // Front
            -0.5,  0.5, .5,   // top left
            -0.5, -0.5, .5,   // bottom left
            0.5, -0.5, .5,   // bottom right
            0.5,  0.5, .5,    // top right


            // Left
            -0.5,  0.5, -.5,   // top left
            -0.5, -0.5, -.5,   // bottom left
            -0.5, -0.5, 0.5,   // bottom right
            -0.5,  0.5, 0.5,    // top right


            // Right
            0.5,  0.5, .5,   // top left
            0.5, -0.5, .5,   // bottom left
            0.5, -0.5, -0.5,   // bottom right
            0.5,  0.5, -0.5,    // top right

            // Top
            -0.5,  0.5, -.5,   // top left
            -0.5, 0.5, .5,   // bottom left
            0.5, 0.5, 0.5,   // bottom right
            0.5,  0.5, -0.5,    // top right


            // Back
            0.5,  -0.5, -.5,   // top left
            -0.5, 0.5, -.5,   // bottom left
            0.5, 0.5, -.5,   // bottom right
            - 0.5, -0.5, -.5,    // top right



            // Bottom
            -0.5,  -0.5, -.5,   // top left      20
            -0.5, -0.5, .5,   // bottom left     21
            0.5, -0.5, 0.5,   // bottom right   22
            0.5,  -0.5, -0.5,    // top right   23
    };


    GLuint vboIndices;
    GLuint indices[] = {
            0, 1, 2,
            0, 2, 3,

            4, 5, 6,
            4, 6, 7,

            8, 9, 10,
            8, 10, 11,

            12, 13, 14,
            12, 14, 15,

            16, 17, 18,
            16, 19, 17,

            20, 22, 21,
            20, 23, 22
    };

    GLuint vboUVs;
    GLfloat uvs[] = {
            0, 1,
            0, 0,
            1, 0,
            1, 1,

            0, 1,
            0, 0,
            1, 0,
            1, 1,

            0, 1,
            0, 0,
            1, 0,
            1, 1,

            0, 1,
            0, 0,
            1, 0,
            1, 1,

            0, 1,
            0, 0,
            1, 0,
            1, 1,


            0, 1,
            0, 0,
            1, 0,
            1, 1,
    };

    GLuint vboNormals;
    GLfloat normals[] = {
            0, 0,1,
            0, 0,1,
            0, 0,1,
            0, 0,1,

            -1, 0, 0,
            -1, 0, 0,
            -1, 0, 0,
            -1, 0, 0,

            1, 0, 0,
            1, 0, 0,
            1, 0, 0,
            1, 0, 0,

            0, 1, 0,
            0, 1, 0,
            0, 1, 0,
            0, 1, 0,

            0, 0,-1,
            0, 0,-1,
            0, 0,-1,
            0, 0,-1,

            0, -1, 0,
            0, -1, 0,
            0, -1, 0,
            0, -1, 0,
    };

    int positionsSizeInBytes = 12 * 6 * 4;
    int uvsSizeInBytes = 8 * 6 * 4;
    int indexSizeInBytes = 6 *6 * 4;
    int normalsSizeInBytes = 12 * 6 * 4;

    glGenBuffers(1, &vboPos);
    glBindBuffer(GL_ARRAY_BUFFER, vboPos);
    glBufferData(GL_ARRAY_BUFFER, positionsSizeInBytes, positions, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glGenBuffers(1, &vboUVs);
    glBindBuffer(GL_ARRAY_BUFFER, vboUVs);
    glBufferData(GL_ARRAY_BUFFER, uvsSizeInBytes, uvs, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &vboNormals);
    glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
    glBufferData(GL_ARRAY_BUFFER, normalsSizeInBytes, normals, GL_STATIC_DRAW);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(2);

    glGenBuffers(1, &vboIndices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSizeInBytes, indices, GL_STATIC_DRAW);

    GLERR

    glBindVertexArray(0);

    numberOfTriangles = 6 * 6;
}
void Geometry::render(GLuint shader, glm::mat4 world, Texture* tex, SceneData* sceneData)
{
    glBindVertexArray(_vao);
    glUseProgram(shader);

    tex->bindToUnit(0);

    glUniform1i(100, 0);

    renderCommon(world, sceneData);

    tex->unbind();

    GLERR
}
void Geometry::renderCommon(glm::mat4 world, SceneData* sd)
{
    glUniformMatrix4fv(6, 1, false, glm::value_ptr(world));
    if (sd->isDepthPass())
    {

        glUniformMatrix4fv(7, 1, false, glm::value_ptr(sd->getShadowCamera()->getViewMatrix()));
        glUniformMatrix4fv(8, 1, false, glm::value_ptr(sd->getShadowCamera()->getProjectionMatrix()));
        glUniform1i(11, 1);
        GLERR
    }
    else
    {
        glUniform1i(11, 0);
        if (sd->getShadowDepthTexture()) {
            sd->getShadowDepthTexture()->bindToUnit(1);
        }
        glUniform1i(104, 0);
        glUniformMatrix4fv(7, 1, false, glm::value_ptr(sd->getCamera()->getViewMatrix()));
        glUniformMatrix4fv(8, 1, false, glm::value_ptr(sd->getCamera()->getProjectionMatrix()));

        if (sd->getShadowCamera()) {
            glUniformMatrix4fv(9, 1, false, glm::value_ptr(sd->getShadowCamera()->getViewMatrix()));
            glUniformMatrix4fv(10, 1, false, glm::value_ptr(sd->getShadowCamera()->getProjectionMatrix()));
        }

        GLERR

        if (sd->isLit()) {
            glUniform1i(102, 1);
            glUniform3fv(103, 1, glm::value_ptr(sd->getLightDirection()));
        }
        else {
            glUniform1i(102, 0);
        }

        if (sd->isFlipUVs()) {
            glUniform1i(12, 1);
        }
        else {
            glUniform1i(12, 0);
        }

        if (sd->isTextureAtlas()) {
            glUniform1i(13, 1);
            glUniform1i(14, sd->getAtlasOffset().x);
            glUniform1i(15, sd->getAtlasOffset().y);
            glUniform1f(16, sd->getAtlasDimension().x);
            glUniform1f(17, sd->getAtlasDimension().y);
        }
        else
        {
            glUniform1i(13, 0);
        }

    }



    glDrawElements(GL_TRIANGLES, numberOfTriangles, GL_UNSIGNED_INT, NULL);
}
void Quad::renderCommon(glm::mat4 world, SceneData* sd) {

    glUniformMatrix4fv(6, 1, false, glm::value_ptr(world));
    if (sd->isDepthPass())
    {
        glUniformMatrix4fv(7, 1, false, glm::value_ptr(sd->getShadowCamera()->getViewMatrix()));
        glUniformMatrix4fv(8, 1, false, glm::value_ptr(sd->getShadowCamera()->getProjectionMatrix()));
        glUniform1i(11, 1);
    }
    else
    {
        if (sd->getShadowDepthTexture())
        {
            sd->getShadowDepthTexture()->bindToUnit(1);
            glUniform1i(104, 0);

            if (sd->isLit()) {
                glUniform1i(102, 1);
                glUniform3fv(103, 1, glm::value_ptr(sd->getLightDirection()));
                glUniformMatrix4fv(9, 1, false, glm::value_ptr(sd->getShadowCamera()->getViewMatrix()));
                glUniformMatrix4fv(10, 1, false, glm::value_ptr(sd->getShadowCamera()->getProjectionMatrix()));
            }
            else {
                glUniform1i(102, 0);
            }
        }
        else {
            glUniform1i(102, 0);
        }
        glUniformMatrix4fv(7, 1, false, glm::value_ptr(sd->getCamera()->getViewMatrix()));
        glUniformMatrix4fv(8, 1, false, glm::value_ptr(sd->getCamera()->getProjectionMatrix()));
        glUniform1i(11, 0);


        if (sd->isUsingTint())
        {
            glUniform1i(105, 1);
            glUniform4fv(106, 1, glm::value_ptr(sd->getTint()));
        }
        else
        {
            glUniform1i(105, 0);
        }

        if (sd->isFlipUVs()) {
            glUniform1i(12, 1);
        }
        else {
            glUniform1i(12, 0);
        }

        if (sd->isTextureAtlas()) {
            glUniform1i(13, 1);
            glUniform1i(14, sd->getAtlasOffset().x);
            glUniform1i(15, sd->getAtlasOffset().y);
            glUniform1f(16, sd->getAtlasDimension().x);
            glUniform1f(17, sd->getAtlasDimension().y);
        }
        else
        {
            glUniform1i(13, 0);
        }

    }



    GLERR

    glDrawElements(GL_TRIANGLES, numberOfTriangles, GL_UNSIGNED_INT, NULL);
    GLERR
}
void Geometry::render(GLuint shader, glm::mat4 world, glm::vec4 color, SceneData* sceneData)
{
    glBindVertexArray(_vao);
    glUseProgram(shader);

    glUniform1i(100, 1);
    glUniform4fv(101, 1, glm::value_ptr(color));


    renderCommon(world, sceneData);

    glUniform1i(100, 0);

    GLERR
}
CubeObj createCube()
{
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vboPos;
    float positions_[] = {
            // Front
            -0.5,  0.5, .5,   // top left
            -0.5, -0.5, .5,   // bottom left
            0.5, -0.5, .5,   // bottom right
            0.5,  0.5, .5,    // top right

            // Left
            // Right
            // Top
            // Back
            // Bottom
    };

    glm::vec3 front1 = { -.5,  .5, .5 };
    glm::vec3 front2 = { -.5, -.5, .5 };
    glm::vec3 front3 = {  .5, -.5, .5 };
    glm::vec3 front4 = {  .5,  .5, .5 };

    int posIndex = 0;
    float positions[60];
    positions[posIndex++] = front1.x;
    positions[posIndex++] = front1.y;
    positions[posIndex++] = front1.z;
    positions[posIndex++] = front2.x;
    positions[posIndex++] = front2.y;
    positions[posIndex++] = front2.z;
    positions[posIndex++] = front3.x;
    positions[posIndex++] = front3.y;
    positions[posIndex++] = front3.z;
    positions[posIndex++] = front4.x;
    positions[posIndex++] = front4.y;
    positions[posIndex++] = front4.z;



    // We create the left face of the cube by rotating 90 degrees to the left
    glm::mat4 mat = glm::rotate(glm::mat4(1), glm::radians<float>(-90), glm::vec3(0, 1, 0));
    glm::vec3 left1 = mat* glm::vec4{ front1, 1 };
    glm::vec3 left2 = mat* glm::vec4 { front2, 1 };
    glm::vec3 left3 = mat * glm::vec4{ front3, 1 };
    glm::vec3 left4 = mat * glm::vec4{ front4, 1 };
    positions[posIndex++] = left1.x;
    positions[posIndex++] = left1.y;
    positions[posIndex++] = left1.z;
    positions[posIndex++] = left2.x;
    positions[posIndex++] = left2.y;
    positions[posIndex++] = left2.z;
    positions[posIndex++] = left3.x;
    positions[posIndex++] = left3.y;
    positions[posIndex++] = left3.z;
    positions[posIndex++] = left4.x;
    positions[posIndex++] = left4.y;
    positions[posIndex++] = left4.z;

    mat = glm::rotate(glm::mat4(1), glm::radians<float>(90), glm::vec3(0, 1, 0));
    glm::vec3 right1 = mat * glm::vec4{ front1, 1 };
    glm::vec3 right2 = mat * glm::vec4{ front2, 1 };
    glm::vec3 right3 = mat * glm::vec4{ front3, 1 };
    glm::vec3 right4 = mat * glm::vec4{ front4, 1 };
    positions[posIndex++] = right1.x;
    positions[posIndex++] = right1.y;
    positions[posIndex++] = right1.z;
    positions[posIndex++] = right2.x;
    positions[posIndex++] = right2.y;
    positions[posIndex++] = right2.z;
    positions[posIndex++] = right3.x;
    positions[posIndex++] = right3.y;
    positions[posIndex++] = right3.z;
    positions[posIndex++] = right4.x;
    positions[posIndex++] = right4.y;
    positions[posIndex++] = right4.z;

    mat = glm::rotate(glm::mat4(1), glm::radians<float>(-90), glm::vec3(1, 0, 0));
    glm::vec3 top1 = mat * glm::vec4{ front1, 1 };
    glm::vec3 top2 = mat * glm::vec4{ front2, 1 };
    glm::vec3 top3 = mat * glm::vec4{ front3, 1 };
    glm::vec3 top4 = mat * glm::vec4{ front4, 1 };
    positions[posIndex++] = top1.x;
    positions[posIndex++] = top1.y;
    positions[posIndex++] = top1.z;
    positions[posIndex++] = top2.x;
    positions[posIndex++] = top2.y;
    positions[posIndex++] = top2.z;
    positions[posIndex++] = top3.x;
    positions[posIndex++] = top3.y;
    positions[posIndex++] = top3.z;
    positions[posIndex++] = top4.x;
    positions[posIndex++] = top4.y;
    positions[posIndex++] = top4.z;

    mat = glm::rotate(glm::mat4(1), glm::radians<float>(-180), glm::vec3(1, 0, 0));
    glm::vec3 back1 = mat * glm::vec4{ front1, 1 };
    glm::vec3 back2 = mat * glm::vec4{ front2, 1 };
    glm::vec3 back3 = mat * glm::vec4{ front3, 1 };
    glm::vec3 back4 = mat * glm::vec4{ front4, 1 };
    positions[posIndex++] = back1.x;
    positions[posIndex++] = back1.y;
    positions[posIndex++] = back1.z;
    positions[posIndex++] = back2.x;
    positions[posIndex++] = back2.y;
    positions[posIndex++] = back2.z;
    positions[posIndex++] = back3.x;
    positions[posIndex++] = back3.y;
    positions[posIndex++] = back3.z;
    positions[posIndex++] = back4.x;
    positions[posIndex++] = back4.y;
    positions[posIndex++] = back4.z;


    GLuint vboIndices;
    GLuint indices[] = {
            0, 1, 2,
            0, 2, 3,

            4, 5, 6,
            4, 6, 7,

            8, 9, 10,
            8, 10, 11,

            12, 13, 14,
            12, 14, 15,

            16, 17, 18,
            16, 18, 19,

            /*    19, 20, 21,
            19, 21, 22*/
    };

    GLuint vboUVs;
    GLfloat uvs[] = {
            0, 1,
            0, 0,
            1, 0,
            1, 1,

            0, 1,
            0, 0,
            1, 0,
            1, 1,

            0, 1,
            0, 0,
            1, 0,
            1, 1,

            0, 1,
            0, 0,
            1, 0,
            1, 1,

            0, 1,
            0, 0,
            1, 0,
            1, 1,


            /*    0, 1,
                0, 0,
                1, 0,
                1, 1,*/
    };

    GLuint vboNormals;
    GLfloat normals[] = {
            0, 0,1,
            0, 0,1,
            0, 0,1,
            0, 0,1,

            -1, 0, 0,
            -1, 0, 0,
            -1, 0, 0,
            -1, 0, 0,

            1, 0, 0,
            1, 0, 0,
            1, 0, 0,
            1, 0, 0,

            0, 1, 0,
            0, 1, 0,
            0, 1, 0,
            0, 1, 0,

            0, 0,-1,
            0, 0,-1,
            0, 0,-1,
            0, 0,-1,

            /*       0, -1, 0,
            0, -1, 0,
            0, -1, 0,
            0, -1, 0,*/
    };

    int positionsSizeInBytes = 12 * 6 * 4;
    int uvsSizeInBytes = 8 * 6 * 4;
    int indexSizeInBytes = 6 * 6 * 4;
    int normalsSizeInBytes = 12 * 6 * 4;

    glGenBuffers(1, &vboPos);
    glBindBuffer(GL_ARRAY_BUFFER, vboPos);
    glBufferData(GL_ARRAY_BUFFER, positionsSizeInBytes, positions, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glGenBuffers(1, &vboUVs);
    glBindBuffer(GL_ARRAY_BUFFER, vboUVs);
    glBufferData(GL_ARRAY_BUFFER, uvsSizeInBytes, uvs, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &vboNormals);
    glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
    glBufferData(GL_ARRAY_BUFFER, normalsSizeInBytes, normals, GL_STATIC_DRAW);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(2);

    glGenBuffers(1, &vboIndices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSizeInBytes, indices, GL_STATIC_DRAW);

    GLERR

    glBindVertexArray(0);

    return { vao, 6 * 6 };
}
void renderCube(CubeObj cube, GLuint shader, glm::mat4 world, glm::vec4 color, SceneData* sd)
{
    glBindVertexArray(cube.vao);
    glUseProgram(shader);

    // We are in the render col without a texture
    // so set the single color flag
    // and pass the respective single color.
    glUniform1i(100, 1);
    glUniform4fv(101, 1, glm::value_ptr(color));


    glUniformMatrix4fv(6, 1, false, glm::value_ptr(world));
    if (sd->isDepthPass())
    {

        glUniformMatrix4fv(7, 1, false, glm::value_ptr(sd->getShadowCamera()->getViewMatrix()));
        glUniformMatrix4fv(8, 1, false, glm::value_ptr(sd->getShadowCamera()->getProjectionMatrix()));
        glUniform1i(11, 1);
    }
    else
    {
        glUniform1i(11, 0);
        sd->getShadowDepthTexture()->bindToUnit(1);
        glUniform1i(104, 0);
        glUniformMatrix4fv(7, 1, false, glm::value_ptr(sd->getCamera()->getViewMatrix()));
        glUniformMatrix4fv(8, 1, false, glm::value_ptr(sd->getCamera()->getProjectionMatrix()));
        glUniformMatrix4fv(9, 1, false, glm::value_ptr(sd->getShadowCamera()->getViewMatrix()));
        glUniformMatrix4fv(10, 1, false, glm::value_ptr(sd->getShadowCamera()->getProjectionMatrix()));

        GLERR

        if (sd->isLit()) {
            glUniform1i(102, 1);
            glUniform3fv(103, 1, glm::value_ptr(sd->getLightDirection()));
        }
        else {
            glUniform1i(102, 0);
        }

        if (sd->isFlipUVs()) {
            glUniform1i(12, 1);
        }
        else {
            glUniform1i(12, 0);
        }

        if (sd->isTextureAtlas()) {
            glUniform1i(13, 1);
            glUniform1i(14, sd->getAtlasOffset().x);
            glUniform1i(15, sd->getAtlasOffset().y);
            glUniform1f(16, sd->getAtlasDimension().x);
            glUniform1f(17, sd->getAtlasDimension().y);
        }
        else
        {
            glUniform1i(13, 0);
        }

    }

    glDrawElements(GL_TRIANGLES, cube.numberOfVertices, GL_UNSIGNED_INT, NULL);

    // Turn single light off
    glUniform1i(100, 0);

    GLERR
}
Circle::Circle(int segments)
{

    glGenVertexArrays(1, &_vao);
    glBindVertexArray(_vao);

    GLuint vboPos;
    float* positions = new float[(segments + 1) * 3];

    // First the center vertex
    positions[0] = { 0 };
    positions[1] = { 0 };
    positions[2] = { 0 };
    float anglePerSegment = (3.14159265359 * 2) / segments;
    int posCounter = 3;

    glm::vec4 baseVec = { 1, 0, 0, 0 };
    std::vector<glm::vec3> vecs;
    for (int i = 0; i < segments; i++) {

        float angle = anglePerSegment * i;
        glm::mat4 mrot = glm::rotate(glm::mat4(1), -angle, glm::vec3(0, 1, 0));
        glm::vec4 vr = mrot * baseVec;
        vecs.push_back(vr);

        glm::vec4 v = { 1, 0, 0 , 0 };
        v = mrot * v;

        positions[posCounter] = v.x;
        positions[posCounter + 1] = v.y;
        positions[posCounter + 2] = v.z;

        posCounter += 3;
    }

    GLuint vboIndices;
    GLuint* indices = new GLuint[segments * 3];
    int stri = 0;
    for (int i = 0; i < segments; i++) {
        indices[stri] = 0;
        indices[stri+1] = i + 1;
        indices[stri+2] = i + 2;
        stri += 3;
    }
    indices[stri - 1] = 1;

    GLuint vboUVs;
    GLfloat uvs[] = {
            0, 1,
            0, 0,
            1, 0,
            1, 1,

            0, 1,
            0, 0,
            1, 0,
            1, 1,

            0, 1,
            0, 0,
            1, 0,
            1, 1,

            0, 1,
            0, 0,
            1, 0,
            1, 1,

            0, 1,
            0, 0,
            1, 0,
            1, 1,


            0, 1,
            0, 0,
            1, 0,
            1, 1,
    };

    GLuint vboNormals;
    GLfloat* normals = new GLfloat[(segments+1) * 3];
    for (int i = 0; i < (segments + 1) * 3; i+=3) {
        normals[i] = 0;
        normals[i + 1] = 1;
        normals[i + 2] = 0;
    }

    int positionsSizeInBytes = (segments + 1) * 3 * 4;
    int uvsSizeInBytes = 8 * 6 * 4;
    int indexSizeInBytes = segments * 3 * 4;
    int normalsSizeInBytes = (segments +1) * 3 * 4;

    glGenBuffers(1, &vboPos);
    glBindBuffer(GL_ARRAY_BUFFER, vboPos);
    glBufferData(GL_ARRAY_BUFFER, positionsSizeInBytes, positions, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glGenBuffers(1, &vboUVs);
    glBindBuffer(GL_ARRAY_BUFFER, vboUVs);
    glBufferData(GL_ARRAY_BUFFER, uvsSizeInBytes, uvs, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &vboNormals);
    glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
    glBufferData(GL_ARRAY_BUFFER, normalsSizeInBytes, normals, GL_STATIC_DRAW);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(2);

    glGenBuffers(1, &vboIndices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSizeInBytes, indices, GL_STATIC_DRAW);

    GLERR

    glBindVertexArray(0);

    numberOfTriangles = (segments +1 )* 3;

}
void Circle::renderCommon(glm::mat4 world, SceneData* sd) {

    glUniformMatrix4fv(6, 1, false, glm::value_ptr(world));
    if (sd->isDepthPass())
    {
        glUniformMatrix4fv(7, 1, false, glm::value_ptr(sd->getShadowCamera()->getViewMatrix()));
        glUniformMatrix4fv(8, 1, false, glm::value_ptr(sd->getShadowCamera()->getProjectionMatrix()));
        glUniform1i(11, 1);
    }
    else
    {
        if (sd->getShadowDepthTexture())
        {
            sd->getShadowDepthTexture()->bindToUnit(1);
            glUniform1i(104, 0);

            if (sd->isLit()) {
                glUniform1i(102, 1);
                glUniform3fv(103, 1, glm::value_ptr(sd->getLightDirection()));
                glUniformMatrix4fv(9, 1, false, glm::value_ptr(sd->getShadowCamera()->getViewMatrix()));
                glUniformMatrix4fv(10, 1, false, glm::value_ptr(sd->getShadowCamera()->getProjectionMatrix()));
            }
            else {
                glUniform1i(102, 0);
            }
        }
        else {
            glUniform1i(102, 0);
        }
        glUniformMatrix4fv(7, 1, false, glm::value_ptr(sd->getCamera()->getViewMatrix()));
        glUniformMatrix4fv(8, 1, false, glm::value_ptr(sd->getCamera()->getProjectionMatrix()));
        glUniform1i(11, 0);


        if (sd->isUsingTint())
        {
            glUniform1i(105, 1);
            glUniform4fv(106, 1, glm::value_ptr(sd->getTint()));
        }
        else
        {
            glUniform1i(105, 0);
        }

        if (sd->isFlipUVs()) {
            glUniform1i(12, 1);
        }
        else {
            glUniform1i(12, 0);
        }

        if (sd->isTextureAtlas()) {
            glUniform1i(13, 1);
            glUniform1i(14, sd->getAtlasOffset().x);
            glUniform1i(15, sd->getAtlasOffset().y);
            glUniform1f(16, sd->getAtlasDimension().x);
            glUniform1f(17, sd->getAtlasDimension().y);
        }
        else
        {
            glUniform1i(13, 0);
        }

    }



    GLERR

    glDrawElements(GL_TRIANGLES, numberOfTriangles, GL_UNSIGNED_INT, NULL);
    GLERR
}


/*void runCode(const std::string& code) {
    //char* code = "print('Hello, World')";

    // Here we load the string and use lua_pcall for run the code
    //Timer t;
    //t.start();
    if (luaL_loadstring(L, code.c_str()) == LUA_OK) {
        //t.stop();
       // SDL_Log("script load: %s", std::to_string(t.get_last_measure(microseconds)).c_str());
        if (lua_pcall(L, 0, 1, 0) == LUA_OK) {
            // If it was executed successfuly we
            // remove the code from the stack
            lua_pop(L, lua_gettop(L));
        }
    }
}*/



/*void runCodeFromFile(const std::string& fileName)
{
    //Timer t;
    //t.start();
    if (luaL_loadfile(L, fileName.c_str()) == LUA_OK) {
        //t.stop();
        //SDL_Log("script load: %s", std::to_string(t.get_last_measure(microseconds)).c_str());
        if (lua_pcall(L, 0, 1, 0) == LUA_OK) {
            lua_pop(L, lua_gettop(L));
        }
        else {
            if (lua_isstring(L, -1)) {
                const char* message = lua_tostring(L, -1);
                lua_pop(L, 1);
                printf("Message from lua: %s\n", message);
            }
        }
    }
}*/


int luafuncs::setClearCol(lua_State *L) {
    //printf("got called the lua change color function\n");
    float r = luaL_checknumber(L, 1);
    float g = luaL_checknumber(L, 2);
    float b = luaL_checknumber(L, 3);

    glClearColor(r, g, b, 1);
    return 0;
}

int luafuncs::clearBackBuffer(lua_State *) {
    GLfloat bkColor[4];
    glGetFloatv(GL_COLOR_CLEAR_VALUE, bkColor);

    glClearBufferfv(GL_COLOR, 0, bkColor);
    return 0;
}

int luafuncs::drawTexture(lua_State *  L) {
    int textId = luaL_checkinteger(L, 1);
    int x = luaL_checknumber(L, 2);
    int y = luaL_checknumber(L, 3);
    int w = luaL_checkinteger(L, 4);
    int h  = luaL_checkinteger(L, 5);

    auto gd = getGameData();
    glViewport(0, 0, gd->screenWidth, gd->screenHeight);

    glm::mat4 mat_proj = glm::ortho<float>(0, gd->screenWidth, 0, gd->screenHeight, 0.1, 10);
    glm::mat4 mat_view = glm::lookAt(glm::vec3(0, 0, 1), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

    glm::mat4 mat_trans = glm::translate(glm::mat4(1), glm::vec3(x + (w/2), y + (h/2), 0));

    // TODO provide the scaling of the actual texture
    // Best via params from Lua
    glm::mat4 mat_scale = glm::scale<float>(glm::mat4(1), glm::vec3(w, h, 1));
    glm::mat4 mat_world = mat_trans * mat_scale;
    Camera cam2(mat_proj, mat_view);
    SceneData sdUI;
    sdUI.setDepthPass(false);
    sdUI.setCamera(&cam2);
    sdUI.setShadowDepthTexture(nullptr);
    sdUI.setTextureAtlas(false);
    getUnitQuad()->render(gd->shaderStore->get("vshader.glsl", "fshader.glsl"), mat_world, textureStore[textId], &sdUI);

    return 0;
}



int luafuncs::createTexture(lua_State* L) {
    const char* textureName = luaL_checkstring(L, 1);
    Texture* t = new Texture(64, 64, TextureUsage::Color);
    int imageChannels, w, h;
    uint8_t*  imgBytes = stbi_load((getAssetBasePath() + textureName).c_str(), &w, &h, &imageChannels, 4);
    t->setData(imgBytes);
    lua_Integer handle = t->handle();
    lua_pushinteger(L, handle);
    textureStore[handle] = t;
    return 1;
}

/*int drawTexture(lua_State*) {
    float x = luaL_checknumber(L, 1);
    float y = luaL_checknumber(L, 2);
    const char* textureName = luaL_checkstring(L, 3);

}*/

void LuaWrapper::runScript(const std::string &scriptFilePath) {
    std::string update_lua_code = getCodeFromFile(scriptFilePath);
    luaL_dostring(L, update_lua_code.c_str());
    lua_pop(L, lua_gettop(L));
}

LuaWrapper::~LuaWrapper() {
    lua_close(L);
}
LuaWrapper::LuaWrapper() {
    L = luaL_newstate();
    luaL_openlibs(L);

    lua_getglobal( L, "package" );
    lua_getfield( L, -1, "path" ); // get field "path" from table at top of stack (-1)
    std::string cur_path = lua_tostring( L, -1 ); // grab path string from top of stack
    cur_path.append( ";" ); // do your path magic here
    cur_path.append( "../assets/?.lua" );
    lua_pop( L, 1 ); // get rid of the string on the stack we just pushed on line 5
    lua_pushstring( L, cur_path.c_str() ); // push the new one
    lua_setfield( L, -2, "path" ); // set the field "path" in table at -2 with value at top of stack
    lua_pop( L, 1 ); // get rid of package table from top of stack

}
std::string LuaWrapper::getCodeFromFile(const std::string &fileName) {
    std::ifstream inFile(fileName, std::ios::in);
    if (!inFile) {
        return 0;
    }

    std::ostringstream code;
    while (inFile.good()) {
        int c = inFile.get();
        if (!inFile.eof()) code << (char)c;
    }
    inFile.close();

    return code.str();
}
void LuaWrapper::runFunction(const std::string &funcName) {
    // We assume that the underlying script for this function has been called, otherwise we
    // will fail at the lua_call...
    // TODO Make this safer?!
    lua_getglobal(L, funcName.c_str());
    lua_pcall(L, 0, 0, 0);
    lua_pop(L, lua_gettop(L));
}

void LuaWrapper::beginNativeFunctionRegistration() {
    lua_newtable(L);
}

void LuaWrapper::endNativeFunctionRegistration(const std::string& luaNamespace) {
    lua_setglobal(L, luaNamespace.c_str());
}

void LuaWrapper::registerNativeFunction(const std::string& luaNamespace, const std::string& functionName, lua_CFunction func) {
    const struct luaL_Reg GameLib[] = {
            {functionName.c_str(), func},
            {nullptr, nullptr}
    };
    luaL_setfuncs(L, GameLib, 0);

}


GameData* getGameData() {
    if (gameData == nullptr) {
        gameData = new GameData();
    }

    return gameData;
}

Quad* getUnitQuad() {
    if (!unitQuad) {
        unitQuad = new Quad();
    }
    return unitQuad;
}

int luafuncs::frameTime(lua_State* L) {
    lua_pushnumber(L, getGameData()->lastFrameTime);
    return 1;
}




FrameBuffer::FrameBuffer(int w, int h)
{
    glGenFramebuffers(1, &_handle);
}

void FrameBuffer::bind()
{

    glBindFramebuffer(GL_FRAMEBUFFER, _handle);

}

bool FrameBuffer::check() {
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        SDL_Log("fb error: %d", status);
        return false;
    }
    else {
        return true;
    }
}

void FrameBuffer::attachColorTexture(Texture* tex)
{
    bind();
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex->handle(), 0);
    activateDrawBuffer();
    unbind();

}

void FrameBuffer::activateDrawBuffer() {
    bind();
    const GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, draw_buffers);
    unbind();
}

void FrameBuffer::deactivateDrawBuffer() {
    bind();
    glDrawBuffer(GL_NONE);
    unbind();
}

void FrameBuffer::deactivateReadBuffer() {
    bind();
    glReadBuffer(GL_NONE);
    unbind();
}

void FrameBuffer::attachDepthTexture(Texture* tex)
{
    bind();
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tex->handle(), 0);
    deactivateDrawBuffer();
    deactivateReadBuffer();
    unbind();

}

void FrameBuffer::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FrameBuffer::clear(GLfloat* clearColor) {
    GLfloat* c = clearColor;
    if (!clearColor) {
        c  = new GLfloat[4]{ 0.0f, 0.01f, 0.01f, 1 };
    }

    glClearBufferfv(GL_COLOR, 0, c);
    GLfloat d = 1;
    glClearBufferfv(GL_DEPTH, 0, &d);
}



int luafuncs::applyPostPrcessEffect(lua_State* L) {
    const char* effectName = luaL_checkstring(L, 1);
    // What do we need to do here?
    // We must take the current offscreen texture
    printf("not yet implemented!\n");
    exit(1);
    return 0;

}

static std::map<int, Mesh*> meshStore;
static int meshId = 0;
int luafuncs::createMesh(lua_State* L) {
    const char* fileName = luaL_checkstring(L, 1);
    Mesh* m = new Mesh(getGameData());

    m->initWithModel(getAssetBasePath() + fileName);

    lua_Integer id = ++meshId;
    lua_pushinteger(L, id);
    return 1;
}



Mesh::Mesh(GameData* gd)
{
    _gameData = gd;
}

static inline glm::mat4 ConvertMatrixToGLMFormat(const aiMatrix4x4& from)
{
    glm::mat4 to;
    //the a,b,c,d in assimp is the row ; the 1,2,3,4 is the column
    to[0][0] = from.a1; to[1][0] = from.a2; to[2][0] = from.a3; to[3][0] = from.a4;
    to[0][1] = from.b1; to[1][1] = from.b2; to[2][1] = from.b3; to[3][1] = from.b4;
    to[0][2] = from.c1; to[1][2] = from.c2; to[2][2] = from.c3; to[3][2] = from.c4;
    to[0][3] = from.d1; to[1][3] = from.d2; to[2][3] = from.d3; to[3][3] = from.d4;
    return to;
}

static inline glm::vec3 GetGLMVec(const aiVector3D& vec)
{
    return glm::vec3(vec.x, vec.y, vec.z);
}

static inline glm::quat GetGLMQuat(const aiQuaternion& pOrientation)
{
    return glm::quat(pOrientation.w, pOrientation.x, pOrientation.y, pOrientation.z);
}

void Mesh::initWithModel(const std::string& modelFileName)
{
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(modelFileName,
                                             aiProcess_Triangulate |
                                             aiProcess_CalcTangentSpace

            //	| aiProcess_FlipUVs
    );

    if (scene == nullptr) {
        printf("error in import of file %s\n", modelFileName.c_str());
        exit(1);
    }

    _vaos.clear();
    _defaultTextures.clear();
    _numbersOfVertices.clear();
    _bones.clear();


    for (int im = 0; im < scene->mNumMeshes; im++) {

        std::vector<float> positions;
        std::vector<float> uvs;
        std::vector<float> normals;
        std::vector<int> indices;
        std::vector<float> tangents;
        std::vector<float> biTangents;
        std::vector<float> weights;
        std::vector<int> boneIds;

        auto* mesh = scene->mMeshes[im];

        for (int x = 0; x < mesh->mNumVertices; x++)
        {
            aiVector3D pos = mesh->mVertices[x];
            positions.push_back(pos.x);
            positions.push_back(pos.y);
            positions.push_back(pos.z);

            if (mesh->HasNormals())
            {
                aiVector3D normal = mesh->mNormals[x];
                normals.push_back(normal.x);
                normals.push_back(normal.y);
                normals.push_back(normal.z);
            }

            if (mesh->HasTextureCoords(0))
            {
                aiVector3D uv = mesh->mTextureCoords[0][x];
                uvs.push_back(uv.x);
                uvs.push_back(uv.y);
            }

            if (mesh->HasTangentsAndBitangents()) {
                aiVector3D tangent = mesh->mTangents[x];
                tangents.push_back(tangent.x);
                tangents.push_back(tangent.y);
                tangents.push_back(tangent.z);

                aiVector3D bitangent = mesh->mBitangents[x];
                biTangents.push_back(bitangent.x);
                biTangents.push_back(bitangent.y);
                biTangents.push_back(bitangent.z);
            }


        }


        printf("num vertices: %d\n", mesh->mNumVertices);
        printf("numFaces: %d\n", mesh->mNumFaces);
        for (int f = 0; f < mesh->mNumFaces; f++)
        {
            aiFace face = mesh->mFaces[f];
            indices.push_back(face.mIndices[0]);
            indices.push_back(face.mIndices[1]);
            indices.push_back(face.mIndices[2]);
        }


        // We want to associate the bone weights
        // with the current vertex.
        // Assimp presents us with a different representation
        // - all bones and for each bone a list of weight entries
        // with a vertexId (an index into the vertex list, our current x counter)
        // and the respective weight.
        // Alternatively
        // we could just reserve space for 4 bones
        // and initialize with boneId = -1 and weight = 0;
        // If later not all slots are used, we are fine.
        // If we need more than 4, this is avoided as we
        // only loop up until index = 4.
        if (mesh->HasBones()) {
            for (int b = 0; b < min(mesh->mNumBones, 4); b++) {

                aiBone* bone = mesh->mBones[b];

            }


        }


        if (mesh->HasBones()) {
            for (int b = 0; b < mesh->mNumBones; b++) {
                aiBone* bone = mesh->mBones[b];


                printf("bone: %s with\n", bone->mName.C_Str());

                aiMatrix4x4 offsetMatrix = bone->mOffsetMatrix;
                printf("offset matrix: row a %f %f %f %f\n", offsetMatrix.a1, offsetMatrix.a2, offsetMatrix.a3, offsetMatrix.a4);
                printf("offset matrix: row b %f %f %f %f\n", offsetMatrix.b1, offsetMatrix.b2, offsetMatrix.b3, offsetMatrix.b4);
                printf("offset matrix: row c %f %f %f %f\n", offsetMatrix.c1, offsetMatrix.c2, offsetMatrix.c3, offsetMatrix.c4);
                printf("offset matrix: row d %f %f %f %f\n", offsetMatrix.d1, offsetMatrix.d2, offsetMatrix.d3, offsetMatrix.d4);
                // TODO store bones in our mesh object.
                // Convert to the glm format before.


                glm::mat4 ofglm = ConvertMatrixToGLMFormat(offsetMatrix);
                Bone* mbone = new Bone();
                int len = strlen(bone->mName.C_Str());
                mbone->name = (char*)malloc(len+1);
                for (int i = 0; i < len+1; i++) {
                    mbone->name[i] = bone->mName.C_Str()[i];
                }
                mbone->offsetMatrix = ofglm;

                // Handle weights
                aiVertexWeight* vertexWeights = bone->mWeights;
                int numWeights = bone->mNumWeights;
                printf("bone: %s with # weights: %u\n", bone->mName.C_Str(), numWeights);
                for (int w = 0; w < numWeights; w++) {
                    printf("\tweight: vId: %u : %f\t (vpos: %f/%f/%f)\n",
                           vertexWeights[w].mVertexId,
                           vertexWeights[w].mWeight,
                           mesh->mVertices[vertexWeights[w].mVertexId].x,
                           mesh->mVertices[vertexWeights[w].mVertexId].y,
                           mesh->mVertices[vertexWeights[w].mVertexId].z);
                    mbone->vertexWeights.push_back({vertexWeights[w].mVertexId, vertexWeights[w].mWeight});
                }


                _bones.push_back(mbone);
            }
        }

        if (scene->HasAnimations()) {
            printf("Number of animations: %d\n", scene->mNumAnimations);
            for (int i = 0; i < scene->mNumAnimations; i++) {
                aiAnimation* animation = scene->mAnimations[i];
                printf("Animation #%d %s\n", i, animation->mName.C_Str());
                printf("\t duration: %f\n", animation->mDuration);
                Animation* kxAnimation = new Animation();
                //kxAnimation->name = TODO
                kxAnimation->duration = animation->mDuration;
                kxAnimation->ticksPerSecond = animation->mTicksPerSecond;

                for (int c = 0; c < animation->mNumChannels; c++) {
                    aiNodeAnim* nodeAnim = animation->mChannels[c];
                    for (int rk = 0; rk < nodeAnim->mNumRotationKeys; rk++) {
                        KeyFrame* keyFrame = new KeyFrame();


                        aiQuatKey quatKey = nodeAnim->mRotationKeys[rk];
                        //keyFrame->bone =
                        keyFrame->time = quatKey.mTime;
                        keyFrame->rotation = glm::quat(quatKey.mValue.w, quatKey.mValue.x, quatKey.mValue.y, quatKey.mValue.z);
                        kxAnimation->keyFrames.push_back(keyFrame);
                        printf("time: %f \t %f \t %f/%f/%f\n", keyFrame->time, keyFrame->rotation.w,
                               keyFrame->rotation.x,
                               keyFrame->rotation.y,
                               keyFrame->rotation.z);

                    }

                }
            }
        }

        // Material and texture import
        if (scene->HasMaterials()) {

            aiMaterial* aimat = scene->mMaterials[mesh->mMaterialIndex];

            printf("Material: %s\n", aimat->GetName().C_Str());
            printf("\t|-- # properties: %d\n", aimat->mNumProperties);
            for (int p = 0; p < aimat->mNumProperties; p++) {
                printf("\t\t|__ %s: %d\n", aimat->mProperties[p]->mKey.C_Str(), aimat->mProperties[p]->mDataLength);
            }
            printf("\t|-- # of diffuse textures: %d\n", aimat->GetTextureCount(aiTextureType::aiTextureType_DIFFUSE));
            printf("\t|-- # of base color textures: %d\n", aimat->GetTextureCount(aiTextureType::aiTextureType_BASE_COLOR));
            printf("\t|-- # of normal map textures: %d\n", aimat->GetTextureCount(aiTextureType::aiTextureType_NORMALS));
            aiString texturePath;
            aiTextureMapMode textureMapMode;
            aiTextureMapping textureMapping;
            aiReturn ret = aimat->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, &texturePath, &textureMapping, NULL, NULL, NULL, &textureMapMode);
            if (ret == aiReturn::aiReturn_SUCCESS)
            {
                printf("Texture Path: %s\n", texturePath.C_Str());
                int imageChannels, w, h;
                uint8_t* imgBytes = stbi_load(texturePath.C_Str(), &w, &h, &imageChannels, 4);
                if (imgBytes) {
                    Texture* texture = new Texture(w, h, TextureUsage::Color);
                    texture->setData(imgBytes);
                    _defaultTextures.push_back(texture);
                }

            }
        }

        // VertexArray creation
        {

            GLuint vao;
            glCreateVertexArrays(1, &vao);
            glBindVertexArray(vao);

            GLuint vbPos;
            glGenBuffers(1, &vbPos);
            glBindBuffer(GL_ARRAY_BUFFER, vbPos);
            glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(GL_FLOAT), positions.data(), GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
            glEnableVertexAttribArray(0);

            GLuint vbUVs;
            if (!uvs.empty()) {
                glGenBuffers(1, &vbUVs);
                glBindBuffer(GL_ARRAY_BUFFER, vbUVs);
                glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(GL_FLOAT), uvs.data(), GL_STATIC_DRAW);
                glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
                glEnableVertexAttribArray(1);
            }

            GLuint vbNormals;
            if (!normals.empty()) {
                glGenBuffers(1, &vbNormals);
                glBindBuffer(GL_ARRAY_BUFFER, vbNormals);
                glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(GL_FLOAT), normals.data(), GL_STATIC_DRAW);
                glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
                glEnableVertexAttribArray(2);
            }

            GLuint vbWeights;
            if (!weights.empty()) {
                glGenBuffers(1, &vbWeights);
                glBindBuffer(GL_ARRAY_BUFFER, vbWeights);
                glBufferData(GL_ARRAY_BUFFER, weights.size() * sizeof(GL_FLOAT), weights.data(), GL_STATIC_DRAW);
                glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
                glEnableVertexAttribArray(3);
            }

            GLuint vbBoneIds;
            if (!boneIds.empty()) {
                glGenBuffers(1, &vbBoneIds);
                glBindBuffer(GL_ARRAY_BUFFER, vbBoneIds);
                glBufferData(GL_ARRAY_BUFFER, boneIds.size() * sizeof(GL_INT), boneIds.data(), GL_STATIC_DRAW);
                glVertexAttribPointer(4, 4, GL_INT, GL_FALSE, 0, (void*)nullptr);
                glEnableVertexAttribArray(4);
            }

            GLuint vbIndices;
            glGenBuffers(1, &vbIndices);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbIndices);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GL_UNSIGNED_INT), indices.data(), GL_STATIC_DRAW);
            _numbersOfVertices.push_back(indices.size());

            GLERR
            glBindVertexArray(0);
            _vaos.push_back(vao);

        }

    }








}

void Mesh::setInstanceWorldMatrices(const std::vector<glm::mat4>& worldMatrices) {

    glBindVertexArray(_vaos[0]);
    // We need to assign the world matrix "instance" attribute here
    unsigned int buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, worldMatrices.size() * sizeof(glm::mat4), worldMatrices.data(), GL_STATIC_DRAW);


    // vertex attributes
    std::size_t vec4Size = sizeof(glm::vec4);
    glEnableVertexAttribArray(5);
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);
    glEnableVertexAttribArray(6);
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(1 * vec4Size));
    glEnableVertexAttribArray(7);
    glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));
    glEnableVertexAttribArray(8);
    glVertexAttribPointer(8, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));

    glVertexAttribDivisor(5, 1);
    glVertexAttribDivisor(6, 1);
    glVertexAttribDivisor(7, 1);
    glVertexAttribDivisor(8, 1);
}

void Mesh::renderInstanced(GLuint shader, std::vector<glm::mat4> worldMatrices, Texture* meshTex, SceneData* sd)
{

    int subMesh = 0;
    for (auto vao : _vaos) {

        glBindVertexArray(vao);
        glUseProgram(shader);



        GLERR

        if (sd->isSingleColor()) {
            glUniform1i(100, 1);
            glUniform4fv(101, 1, glm::value_ptr(sd->getSingleColor()));
        }
        else {
            glUniform1i(100, 0);
            // We always override with a passed in texture,
            // hopefully the caller knows why he
            // passes in a specific texture.
            if (meshTex) {
                meshTex->bindToUnit(0);
            }
            else
            {
                // Put some impossible color here and render single colored.
                glUniform1i(100, 1);
                glUniform4fv(101, 1, glm::value_ptr(glm::vec4(1, 0, 1, 1)));
            }
        }

        // No skeletal meshes for now
        glUniform1i(104, 0);
        glUniform1i(18, 0);

        // The world matrix are provided per instance and via the respective attribute, see above.
        //glUniformMatrix4fv(6, 1, false, glm::value_ptr(world));
        if (sd->isDepthPass())
        {

            glUniformMatrix4fv(7, 1, false, glm::value_ptr(sd->getShadowCamera()->getViewMatrix()));
            glUniformMatrix4fv(8, 1, false, glm::value_ptr(sd->getShadowCamera()->getProjectionMatrix()));
            glUniform1i(11, 1);
        }
        else
        {
            glUniform1i(11, 0);

            if (sd->getShadowDepthTexture()) {
                sd->getShadowDepthTexture()->bindToUnit(1);
                glUniformMatrix4fv(9, 1, false, glm::value_ptr(sd->getShadowCamera()->getViewMatrix()));
                glUniformMatrix4fv(10, 1, false, glm::value_ptr(sd->getShadowCamera()->getProjectionMatrix()));
                glUniform1i(104, 0);
            }
            else {
                glUniform1i(104, 1);
            }


            glUniformMatrix4fv(7, 1, false, glm::value_ptr(sd->getCamera()->getViewMatrix()));
            glUniformMatrix4fv(8, 1, false, glm::value_ptr(sd->getCamera()->getProjectionMatrix()));


            GLERR

            // TODO unify light direction with the shadow camera forward vector.
            if (sd->isLit())
            {
                glUniform1i(102, 1);
                glUniform3fv(103, 1, glm::value_ptr(sd->getLightDirection()));
            }
            else
            {
                glUniform1i(102, 0);
            }

            if (sd->isFlipUVs())
            {
                glUniform1i(12, 1);
            }
            else {
                glUniform1i(12, 0);
            }

            if (sd->isTextureAtlas())
            {
                glUniform1i(13, 1);
                glUniform1i(14, sd->getAtlasOffset().x);
                glUniform1i(15, sd->getAtlasOffset().y);
                glUniform1f(16, sd->getAtlasDimension().x);
                glUniform1f(17, sd->getAtlasDimension().y);
            }
            else
            {
                glUniform1i(13, 0);
            }
        }

        //glDrawElements(GL_TRIANGLES, _numbersOfVertices[subMesh], GL_UNSIGNED_INT, NULL);
        glDrawElementsInstanced(
                GL_TRIANGLES, _numbersOfVertices[subMesh], GL_UNSIGNED_INT, 0, worldMatrices.size()
        );

        if (meshTex) {
            meshTex->unbind();
        }

        glUniform1i(104, 0);	// isSkeletalAnimation off
        glUniform1i(18, 0);		// noShadow off



        GLERR
        subMesh++;
    }


}

void Mesh::render(GLuint shader, glm::mat4 world, Texture* meshTex, SceneData* sd)
{
    int subMesh = 0;
    for (auto vao : _vaos) {

        glBindVertexArray(vao);

        /*GLuint shaderHandle = _gameData->shaderStore->get("vshader.glsl", "fshader.glsl");*/
        glUseProgram(shader);


        if (sd->isSingleColor()) {
            glUniform1i(100, 1);
            glUniform4fv(101, 1, glm::value_ptr(sd->getSingleColor()));
        }
        else {
            glUniform1i(100, 0);
            // We always override with a passed in texture,
            // hopefully the caller knows why he
            // passes in a specific texture.
            if (meshTex) {
                meshTex->bindToUnit(0);
            }
            else if (_defaultTextures.size() > 0 && _defaultTextures[subMesh] != nullptr) {
                // Otherwise we expext to have the default textures
                // ready.
                _defaultTextures[subMesh]->bindToUnit(0);
            }
            else {
                // Put some impossible color here and render single colored.
                glUniform1i(100, 1);
                glUniform4fv(101, 1, glm::value_ptr(glm::vec4(1, 0, 1, 1)));
            }
        }


        // For now we don't apply any skeletal mesh magic here...!
        /*if (_isSkeletalMesh) {
            glUniform1i(18, 1);
            for (int i = 0; i < 4; i++) {
                char buf[150];
                sprintf_s(buf, 150, "boneTransforms[%d]", i);
                GLuint loc = glGetUniformLocation(shader, buf);
                if (i == 0) {
                    glUniformMatrix4fv(loc, 1, false, glm::value_ptr(glm::rotate(glm::mat4(1), glm::radians(24.0f), glm::vec3(0, 1, 0))));
                }
                else {
                    glUniformMatrix4fv(loc, 1, false, glm::value_ptr(glm::mat4(1)));
                }

                GLERR
            }
        }
        else {*/
        glUniform1i(18, 0);
        //}

        glUniformMatrix4fv(6, 1, false, glm::value_ptr(world));
        if (sd->isDepthPass())
        {

            glUniformMatrix4fv(7, 1, false, glm::value_ptr(sd->getShadowCamera()->getViewMatrix()));
            glUniformMatrix4fv(8, 1, false, glm::value_ptr(sd->getShadowCamera()->getProjectionMatrix()));
            glUniform1i(11, 1);
        }
        else
        {
            glUniform1i(11, 0);

            if (sd->getShadowDepthTexture()) {
                sd->getShadowDepthTexture()->bindToUnit(1);
                glUniformMatrix4fv(9, 1, false, glm::value_ptr(sd->getShadowCamera()->getViewMatrix()));
                glUniformMatrix4fv(10, 1, false, glm::value_ptr(sd->getShadowCamera()->getProjectionMatrix()));
                glUniform1i(104, 0);
            }
            else {
                glUniform1i(104, 1);
            }


            glUniformMatrix4fv(7, 1, false, glm::value_ptr(sd->getCamera()->getViewMatrix()));
            glUniformMatrix4fv(8, 1, false, glm::value_ptr(sd->getCamera()->getProjectionMatrix()));


            GLERR

            // TODO unify light direction with the shadow camera forward vector.
            if (sd->isLit())
            {
                glUniform1i(102, 1);
                glUniform3fv(103, 1, glm::value_ptr(sd->getLightDirection()));
            }
            else
            {
                glUniform1i(102, 0);
            }

            if (sd->isFlipUVs())
            {
                glUniform1i(12, 1);
            }
            else {
                glUniform1i(12, 0);
            }

            if (sd->isTextureAtlas())
            {
                glUniform1i(13, 1);
                glUniform1i(14, sd->getAtlasOffset().x);
                glUniform1i(15, sd->getAtlasOffset().y);
                glUniform1f(16, sd->getAtlasDimension().x);
                glUniform1f(17, sd->getAtlasDimension().y);
            }
            else
            {
                glUniform1i(13, 0);
            }
        }

        glDrawElements(GL_TRIANGLES, _numbersOfVertices[subMesh], GL_UNSIGNED_INT, NULL);

        if (meshTex) {
            meshTex->unbind();
        }
        else {
            if (_defaultTextures.size() > 0) {
                _defaultTextures[subMesh]->unbind();
            }

        }


        // Reset this state to be on the safe side.
        // After us, other render codes might
        // run which do not explicitely reset these settings.
        glUniform1i(104, 0);	// isSkeletalAnimation off
        glUniform1i(18, 0);		// noShadow off

        GLERR
        subMesh++;

    }


}



#endif

#endif //KOENIG012022_KXGAMELIB_H
