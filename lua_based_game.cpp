//
// Created by mgrus on 31/12/2021.
// This is an implementation of a "Game" which
// is purely based on Lua scripting.
//

#include "kxgamelib.h"


// This method must be implemented in one file within the project.
// It is called by the framework and it will use the game instance you provide
// here.
Game* getGame() {
        return new LuaBasedGame();
}

void DummyLevel::cleanUp() {

}

bool DummyLevel::isFinished() {
    return false;
}

void DummyLevel::render() {

    glClearColor(0.0, 0.0, 0.0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Render to the offscreen buffer
    {
        glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 3, -1, "OffscreenBuffer");

        _mainFBO->bind();
        _mainFBO->clear(nullptr);

        /*SceneData sd;
        sd.setCamera(_camera);
        sd.setDepthPass(false);
        sd.setLit(false);*/
        glViewport(0, 0, _gameData->logicalScreenWidth, _gameData->logicalScreenHeight);

        //sd.setLit(true);

        lua->runFunction("render");

        glPopDebugGroup();
    }

    lua->runFunction("postRender");

    // Render offscreen contents to a fullscreen quad
    {
        glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 1, -1, "Fullscreen Quad");
        glEnable(GL_DEPTH_TEST);
        _mainFBO->unbind();
        glViewport(_gameData->screenWidth/2 - _gameData->logicalScreenWidth/2,
                   _gameData->screenHeight/2 - _gameData->logicalScreenHeight/2,
                   _gameData->logicalScreenWidth, _gameData->logicalScreenHeight);

        {
            glm::mat4 mat_view = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
            glm::mat4 mat_proj = glm::ortho<float>(-.5, .5, -.5, .5, 0.1, 50);
            glm::mat4 mat_world = glm::scale<float>(glm::mat4(1), glm::vec3(1, 1, 1));
            Camera cam(mat_proj, mat_view);
            SceneData sd;
            sd.setDepthPass(false);
            sd.setCamera(&cam);
            sd.setFlipUVs(false);
            _unitQuad->render(shader, mat_world, _mainColorTex, &sd);
            GLERR
        }
        glPopDebugGroup();
    }



}

void DummyLevel::init(void *data) {
    GameData* gd = (GameData*) data;
    _mainFBO = new FrameBuffer(gd->logicalScreenWidth, gd->logicalScreenHeight);
    _mainColorTex = new Texture(gd->logicalScreenWidth, gd->logicalScreenHeight, TextureUsage::Color);
    _mainDepthTex = new Texture(gd->logicalScreenWidth, gd->logicalScreenHeight, TextureUsage::Depth);

    _mainFBO->attachColorTexture(_mainColorTex);
    _mainFBO->attachDepthTexture(_mainDepthTex);
    _mainFBO->activateDrawBuffer();
    if (!_mainFBO->check()) {
        exit(4);
    }

    _shadowDepthFBO = new FrameBuffer(1024, 1024);
    _shadowDepthTex = new Texture(1024, 1024, TextureUsage::Depth);
    _shadowDepthFBO->attachDepthTexture(_shadowDepthTex);
    _shadowDepthFBO->deactivateDrawBuffer();
    _shadowDepthFBO->deactivateReadBuffer();
}

DummyLevel::DummyLevel(GameData *gameData) : Level(gameData) {
    lua = (LuaWrapper*) gameData->userData1;
    _unitQuad = new Quad();
    shader = _gameData->shaderStore->get("vshader.glsl", "fshader.glsl");

}

Level* LuaBasedGame::getNextLevel() {
    // In a real game we might have a list of levels and depending
    // which finished last, we set the next level accordingly.
    return new DummyLevel(gameData);
}

void LuaBasedGame::init(GameData *gameData) {
    Game::init(gameData);
    lua = new LuaWrapper();
    lua->beginNativeFunctionRegistration();
    lua->registerNativeFunction("GameLib", "setClearCol", luafuncs::setClearCol);
    lua->registerNativeFunction("GameLib", "clearBackBuffer", luafuncs::clearBackBuffer);
    lua->registerNativeFunction("GameLib", "createTexture", luafuncs::createTexture);
    lua->registerNativeFunction("GameLib", "drawTexture", luafuncs::drawTexture);
    lua->registerNativeFunction("GameLib", "frameTime", luafuncs::frameTime);
    lua->registerNativeFunction("GameLib", "createMesh", luafuncs::createMesh);
    lua->endNativeFunctionRegistration("GameLib");
    lua->runScript("../assets/update.lua");
    lua->runScript("../assets/render.lua");

    gameData->userData1 = lua;





}

// How to override the logical screen size and
// setting the fullscreen mode.
// If we are in fullscreen mode and the logical screen size
// is smaller than the actual screen,
// the game will be rendered into a centered viewport of the
// given logical size as returned from this method.
// If we are windowed, the engine will create a window of the desired size.
ScreenData LuaBasedGame::getScreenData() {
    return { 1920, 1080, false };
}

void DummyLevel::update(float frameTimeInSeconds, std::vector<SDL_Event> events) {
    for (auto event : events) {
        // Reload the lua update script(s)
        // TODO should we reload all scripts here?
        if (event.key.keysym.sym == SDLK_r) {
            if (SDL_GetModState() & KMOD_CTRL) {
                lua->runScript("../assets/update.lua");
                lua->runScript("../assets/render.lua");
            }
        }
    }

    lua->runFunction("update");


}



void Game::init(GameData *gd) {
    this->gameData = gd;

}

ScreenData Game::getScreenData() {
    return ScreenData();
}


