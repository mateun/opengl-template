

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <vector>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <algorithm>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <dsound.h>

#define KXGAMELIB_IMPL
#include "kxgamelib.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2main.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "assimp-vc142-mt.lib")
#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "lua.lib")

Game* game = nullptr;
Level* currentLevel = nullptr;

LPDIRECTSOUND8 initAudio(HWND hwnd) {
    LPDIRECTSOUNDBUFFER sbPrimary;
    LPDIRECTSOUND8 dsound8;

    HRESULT hr = DirectSoundCreate8(NULL, &dsound8, NULL);
    if (FAILED(hr)) {
        SDL_Log("sound error: %d", hr);
        exit(99);
    }

    dsound8->SetCooperativeLevel(hwnd, DSSCL_PRIORITY);
    DSBUFFERDESC dd;
    memset(&dd, 0, sizeof(DSBUFFERDESC));
    dd.dwSize = sizeof(DSBUFFERDESC);
    dd.dwFlags = DSBCAPS_PRIMARYBUFFER;
    dd.dwBufferBytes = 0;
    dd.lpwfxFormat = NULL;

    hr = dsound8->CreateSoundBuffer(&dd, &sbPrimary, NULL);
    if (FAILED(hr)) {
        SDL_Log("sound buffer error %d", hr);
        exit(1);
    }


    WAVEFORMATEX wfx;
    memset(&wfx, 0, sizeof(WAVEFORMATEX));
    wfx.wFormatTag = WAVE_FORMAT_PCM;
    wfx.nChannels = 1;
    wfx.nSamplesPerSec = 44100;
    wfx.wBitsPerSample = 16;
    wfx.nBlockAlign = (wfx.wBitsPerSample * wfx.nChannels)/8;
    wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign ;

    hr = sbPrimary->SetFormat(&wfx);
    if (FAILED(hr)) {
        SDL_Log("error while setting format for primary soundbuffer");
        exit(1);
    }

    // Secondary buffer
    LPDIRECTSOUNDBUFFER buf;
    memset(&dd, 0, sizeof(DSBUFFERDESC));
    dd.dwSize = sizeof(DSBUFFERDESC);
    dd.dwFlags = DSBCAPS_STATIC;
    dd.dwBufferBytes = wfx.nSamplesPerSec * 2 * 2;		// 1 second for 2 bytes per sample (16bit)
    dd.lpwfxFormat = &wfx;

    hr = dsound8->CreateSoundBuffer(&dd, &buf, NULL);
    if (FAILED(hr)) {
        SDL_Log("secondary sound buffer error %d", hr);
        exit(110);
    }

    uint16_t* audio_ptr_1;
    uint16_t* audio_ptr_2;
    DWORD audio_l_1;
    DWORD audio_l_2;

    hr = buf->Lock(0, dd.dwBufferBytes, (void**)&audio_ptr_1, &audio_l_1, (void**)&audio_ptr_2, &audio_l_2, DSBLOCK_ENTIREBUFFER);
    if (FAILED(hr)) {
        SDL_Log("secondary sound buffer lock error %d", hr);
        exit(111);
    }
    bool up = true;
    int freq = 262;
    int samplesPerCycle = wfx.nSamplesPerSec / freq;
    int currentSample = 0;
    int duration = 2;

    while (currentSample < wfx.nSamplesPerSec * duration) {

        for (int i = 0; i < samplesPerCycle / 2; i++) {
            if (currentSample < audio_l_1/2) {
                audio_ptr_1[currentSample] = 10000;
            }

            currentSample++;

        }

        for (int i = 0; i < samplesPerCycle / 2; i++) {
            if (currentSample < audio_l_1/2) {
                audio_ptr_1[currentSample] = 500;
            }
            currentSample++;
        }

    }



    return dsound8;

    //buf->Play(0, 0, 0);


}

extern Game* getGame();

int main(int argc, char** args) {

    int logicalScreenWidth = 0;
    int logicalScreenHeight = 0;
    int screenWidth = 0;
    int screenHeight = 0;
    bool fullScreen = false;

    if (SDL_Init(SDL_INIT_VIDEO == -1)) {
        SDL_Log("error initializing. %s", SDL_GetError());
        return 1;
    }

    SDL_Log("SDL init ok.");

    game = getGame();
    auto sd = game->getScreenData();
    logicalScreenWidth = sd.width;
    logicalScreenHeight = sd.height;
    fullScreen = sd.fullScreen;

    if (fullScreen) {
        screenWidth = GetSystemMetrics (SM_CXSCREEN);
        screenHeight = GetSystemMetrics (SM_CYSCREEN);
    } else {
        screenWidth = logicalScreenWidth;
        screenHeight = logicalScreenHeight;
    }


    Uint32 windowFlags = SDL_WINDOW_OPENGL;
    if (fullScreen) {
        windowFlags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
    }
    SDL_Window* window = SDL_CreateWindow("test 0.0.1", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, windowFlags);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GLContext glContext = SDL_GL_CreateContext(window);

    int maj, min;
    glGetIntegerv(GL_MAJOR_VERSION, &maj);
    glGetIntegerv(GL_MINOR_VERSION, &min);

    SDL_Log("context: %d.%d", maj, min);

    SDL_GLContext ctx = SDL_GL_CreateContext(window);
    SDL_GL_SetSwapInterval(1);

    GLenum r = glewInit();
    if (r != GLEW_OK) {
        return 2;
    }

    SDL_Log("glew init ok.");

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glViewport(0, 0, screenWidth, screenHeight);


    ShaderStore shaderStore;
    shaderStore.put("vshader.glsl", "fshader.glsl");

    SDL_Log("shader compilation ok.");

    uint64_t perfFreq = SDL_GetPerformanceFrequency();
    uint64_t startCount = SDL_GetPerformanceCounter();
    uint64_t durationInTicks = 0;

    GameData* gameData = getGameData();
    gameData->screenWidth = screenWidth;
    gameData->screenHeight= screenHeight;
    gameData->logicalScreenWidth = logicalScreenWidth;
    gameData->logicalScreenHeight = logicalScreenHeight;
    gameData->fullScreen = fullScreen;
    gameData->shaderStore = &shaderStore;
    gameData->sdlWindow = window;
    gameData->performanceFrequency = perfFreq;

    // TODO
    // for now we replace this with imgui and later for ingame
    // things with our own implementation of GUI ops.
    //initUIImmediateSystem(&gameData);

    LPDIRECTSOUND8 dsound8 = initAudio(gameData->nativeWindowsHandle());
    gameData->dsound8 = dsound8;

    game->init(gameData);
    currentLevel = game->getNextLevel();
    currentLevel->init(gameData);

    bool running = true;
    std::vector<SDL_Event> frameEvents;
    while (running) {
        frameEvents.clear();
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = false;
                break;
            }

            if (event.type == SDL_KEYDOWN) {
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    running = false;
                    break;
                }


            }
            frameEvents.push_back(event);
        }

        int mouseX, mouseY;
        SDL_GetMouseState(&mouseX, &mouseY);

        float frameTimeInSeconds = (float)durationInTicks / perfFreq;
        gameData->lastFrameTime = frameTimeInSeconds;

        if (currentLevel)  {
            if (currentLevel->isFinished()) {
                Level* nextLevel = game->getNextLevel();
                currentLevel->cleanUp();
                currentLevel = nextLevel;
                currentLevel->init(nullptr);
            } else {
                //updateUIEvents(mouseX, mouseY, frameEvents);
                currentLevel->update(frameTimeInSeconds, frameEvents);
                currentLevel->render();
            }

        }

        SDL_GL_SwapWindow(window);
        uint64_t endCount = SDL_GetPerformanceCounter();
        durationInTicks = endCount - startCount;

        startCount = SDL_GetPerformanceCounter();
    }



    //destroyUIImmediateSystem();
    SDL_GL_DeleteContext(ctx);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;


}
