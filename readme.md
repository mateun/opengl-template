# OpenGL Game Template

This CMake based template shall provide a quickstart to create OpenGL based
games. 

It consists of some library functions in the single file kxgamelib.h. 

The idea is that for a specific game you add your game specific files which 
descend from "Level" and you enter them in the main.cpp "getCurrentLevel" method.
